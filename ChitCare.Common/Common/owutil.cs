﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
   public class owutil
    {
        #region Region for conversions between database variables to Dotnet variables
        public static string nvl(object inval)
        {
            if (inval == System.DBNull.Value)
                return String.Empty;
            else
                return System.Convert.ToString(inval);
        }
        public static char nvlchar(object inval)
        {
            if (inval == System.DBNull.Value)
                return Char.Parse("");
            else
                return System.Convert.ToChar(inval);
        }
        public static int nvlint(object inval)
        {
            if (inval == System.DBNull.Value)
                return 0;
            else
                return System.Convert.ToInt32(inval);
        }
        public static short nvlshort(object inval)
        {
            if (inval == System.DBNull.Value)
                return 0;
            else
                return System.Convert.ToInt16(inval);
        }
        public static long nvllong(object inval)
        {
            if (inval == System.DBNull.Value)
                return 0;
            else
                return System.Convert.ToInt64(inval);
        }
        public static double nvldbl(object inval)
        {
            if (inval == System.DBNull.Value)
                return 0;
            else
            {
                return System.Convert.ToDouble(inval);
            }
        }

        public static DateTime nvldate(object inval)
        {
            if (inval == System.DBNull.Value)
            {
                DateTime dt = new DateTime();
                return dt;
            }
            else
                return Convert.ToDateTime(inval);
        }
        public static byte nvlbyte(object inval)
        {
            if (inval == System.DBNull.Value)
                return 0;
            else
                return Convert.ToByte(inval);
        }
        public static bool nvlbool(object inval)
        {
            if (inval == System.DBNull.Value)
                return false;
            else
                return Convert.ToBoolean(inval);
        }
        public static byte[] nvlbytearr(object inval)
        {
            return (byte[])inval;
        }
        #endregion
    }
}
