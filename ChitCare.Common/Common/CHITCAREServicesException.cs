﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constants
{
   public class CHITCAREServicesException :Exception
    {
        public string StatusCode { get; set; }

        public CHITCAREServicesException(string statusCode = "500") : base(statusCode) 
        {
            StatusCode = statusCode;
        }

        public CHITCAREServicesException(Exception ex, string statusCode = "500") : base(statusCode, ex)
        {
            StatusCode = statusCode;
        }
    }
}
