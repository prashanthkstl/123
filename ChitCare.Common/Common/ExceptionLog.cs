﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
namespace Constants
{
   public class ExceptionLog
    {
        public static void ErrorsEntry(string ErrorSource)
        {
            StreamWriter streamWriter = File.AppendText(ConfigurationManager.AppSettings["LogFile"].ToString());
            streamWriter.WriteLine("====================" + DateTime.Now.ToLongDateString() + "  " + DateTime.Now.ToLongTimeString() + "====================");
            streamWriter.WriteLine(ErrorSource.ToString());
            streamWriter.Flush();
            streamWriter.Close();
        }
    }
}
