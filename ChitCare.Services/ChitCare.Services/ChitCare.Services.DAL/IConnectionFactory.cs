﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.Services.DAL
{
   public interface IConnectionFactory : IDisposable
    {
        IDbConnection GetConnection { get; }
    }
}
