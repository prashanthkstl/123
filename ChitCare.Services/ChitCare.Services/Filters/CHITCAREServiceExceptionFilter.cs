﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using Constants;




namespace ChitCare.Services.Filters
{
    public class CHITCAREServiceExceptionFilter : ExceptionFilterAttribute
    {

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            HttpResponseMessage response = null;

            if (actionExecutedContext.Exception is CHITCAREServicesException)
            {
                response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent((actionExecutedContext.Exception as CHITCAREServicesException).StatusCode)
                };
            }
            else
            {
                string exceptionMessage = string.Empty;

                if (actionExecutedContext.Exception.InnerException == null)
                    exceptionMessage = actionExecutedContext.Exception.Message;
                else
                    exceptionMessage = actionExecutedContext.Exception.InnerException.Message;

                ExceptionLog.ErrorsEntry("Exception occurred. Exception details: " + exceptionMessage + " base exception details: " + actionExecutedContext.Exception.GetBaseException().ToString());

                response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    //Content = new StringContent(Messages.Error_10001)
                };
            }

            actionExecutedContext.Response = response;

        }
    }
}
