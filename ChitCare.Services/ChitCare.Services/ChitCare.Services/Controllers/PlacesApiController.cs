﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.Services.Controllers
{
    [RoutePrefix("Places")]
    public class PlacesApiController : ChitCareApiController
    {
        #region Country Region
        [HttpGet]

        [Route("GetCountries")]
        public IHttpActionResult GetCountries()
        {
            return Ok(Manager.PlacesManager.GetCountries());
        }

        [HttpPost]

        [Route("CountryInsert")]
        public void CountryCreate(Countries countries)
        {
            Manager.PlacesManager.CountryInsert(countries);
        }
        [HttpGet]

        [Route("CountryEdit")]
        public IHttpActionResult CountryEdit(int CountryId)
        {
            return Ok(Manager.PlacesManager.CountryEdit(CountryId));
        }

        [HttpPost]
        [Route("DeleteCountry")]
        public void DeleteCountry(Countries countries)
        {
            Manager.PlacesManager.DeleteCountry(countries);
        }
        [HttpPost]
        [Route("UpdateCountry")]
        public void UpdateCountry(Countries countries)
        {
            Manager.PlacesManager.CountryUpdate(countries);
        }
#endregion

        #region States Region

        [HttpGet]
        [Route("GetStates")]
        public IHttpActionResult GetStates()
        {
            return Ok(Manager.PlacesManager.GetStates());
        }
        [HttpPost]
        [Route("StateInsert")]
        public void StateInsert(States states)
        {
            Manager.PlacesManager.StateInsert(states);
        }

        [HttpGet]
        [Route("StateView")]

        public IHttpActionResult StateView(int CountryId)
        {
            return Ok(Manager.PlacesManager.StateView(CountryId));
        }

        [HttpGet]

        [Route("StateEdit")]
        public IHttpActionResult StateEdit(int StateId)
        {
            return Ok(Manager.PlacesManager.StateEdit(StateId));
        }
        [HttpPost]
        [Route("DeleteState")]
        public void DeleteState(States states)
        {
            Manager.PlacesManager.DeleteState(states);
        }
        //[HttpPost]
        //[Route("UpdateState")]
        //public void UpdateState(States states)
        //{
        //    Manager.PlacesManager.StateUpdate(states);
        //}
        #endregion

        #region City Region

        [HttpGet]
        [Route("GetCities")]
        public IHttpActionResult GetCities()
        {
            return Ok(Manager.PlacesManager.GetCities());
        }
        [HttpPost]
        [Route("CityInsert")]
        public void CityInsert(Cities cities)
        {
            Manager.PlacesManager.CitiesInsert(cities);
        }

        [HttpGet]
        [Route("CityView")]

        public IHttpActionResult CityView(int CityDistrictId, string CityName)
        {
            return Ok(Manager.PlacesManager.CityView(CityDistrictId, CityName));
        }

        [HttpGet]

        [Route("CityEdit")]
        public IHttpActionResult CityEdit(int CityId)
        {
            return Ok(Manager.PlacesManager.CityEdit(CityId));
        }
        [HttpPost]
        [Route("DeleteCity")]
        public void DeleteCity(Cities cities)
        {
            Manager.PlacesManager.DeleteCity(cities);
        }
        [HttpPost]
        [Route("UpdateCity")]
        public void UpdateCity(Cities cities)
        {
            Manager.PlacesManager.CitiesUpdate(cities);
        }
        #endregion

        #region District Region

        [HttpGet]
        [Route("GetDistrict")]
        public IHttpActionResult GetDistrict()
        {
            return Ok(Manager.PlacesManager.GetDistricts());
        }
        [HttpPost]
        [Route("DistrictInsert")]
        public void DistrictInsert(Districts dist)
        {
            Manager.PlacesManager.DistInsert(dist);
        }

        [HttpGet]
        [Route("DistrictView")]

        public IHttpActionResult DistrictView(int StateId)
        {
            return Ok(Manager.PlacesManager.DistrictView(StateId));
        }


        [HttpPost]

        [Route("DeleteDistrict")]
        public void DeleteDistrict(Districts dist)
        {
            Manager.PlacesManager.DeleteDist(dist);
        }

        [HttpGet]

        [Route("DistrictEdit")]
        public IHttpActionResult DistrictEdit(int DistrictId)
        {
            return Ok(Manager.PlacesManager.DistrictEdit(DistrictId));
        }
        [HttpPost]
        [Route("UpdateDistrict")]
        public void UpdateDistrict(Districts dist)
        {
            Manager.PlacesManager.DistUpdate(dist);
        }
        #endregion

        #region Route Region
        [HttpPost]
        [Route("RouteInsert")]
        public void RouteInsert(Routes route)
        {
            Manager.PlacesManager.RouteInsert(route);
        }

        [HttpGet]
        [Route("RouteView")]

        public IHttpActionResult RouteView(int BranchId)
        {
            return Ok(Manager.PlacesManager.RouteView(BranchId));
        }

        [HttpGet]

        [Route("RouteEdit")]
        public IHttpActionResult RouteEdit(int RouteID)
        {
            return Ok(Manager.PlacesManager.RouteEdit(RouteID));
        }

        #endregion

        #region Area Region
        [HttpPost]
        [Route("AreaEntry")]
        public void AreaEntry(Areas area)
        {
            Manager.PlacesManager.AreaEntry(area);
        }

        [HttpGet]
        [Route("AreaView")]

        public IHttpActionResult AreaView(int RouteID)
        {
            return Ok(Manager.PlacesManager.AreaView(RouteID));
        }


        [HttpGet]

        [Route("AreaEdit")]
        public IHttpActionResult AreaEdit(int AreaID)
        {
            return Ok(Manager.PlacesManager.AreaEdit(AreaID));
        }


        [HttpGet]
        [Route("GetRoutes")]
        public IHttpActionResult GetRoutes()
        {
            return Ok(Manager.PlacesManager.GetRoutes());
        }
        #endregion
    }
}
