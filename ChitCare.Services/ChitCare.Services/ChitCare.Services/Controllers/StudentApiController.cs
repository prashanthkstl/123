﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.Services.Controllers
{
    [RoutePrefix("Student")]
    public class StudentApiController : ChitCareApiController
    {
        [HttpGet]
        [Route("GetStudents")]
        public IHttpActionResult GetStudents()
        {
            return Ok(Manager.StudentManager.GetStudent());
        }
        [HttpPost]
        [Route("Insert")]
        public void Create(Student student)
        {
            Manager.StudentManager.Save(student);
        }
        [HttpPost]
        [Route("Delete")]
        public void DeleteStudent(Student student)
        {
            Manager.StudentManager.DeleteMultiple(student);
        }
        [HttpPost]
        [Route("Update")]
        public void UpdateStudent(Student student)
        {
            Manager.StudentManager.Update(student);
        }

    }
}
