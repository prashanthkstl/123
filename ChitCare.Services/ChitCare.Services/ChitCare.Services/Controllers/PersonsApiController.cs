﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.Services.Controllers
{
    [RoutePrefix("Persons")]
    public class PersonsApiController : ChitCareApiController
    {
        [HttpGet]
        [Route("AutoList")]
        public IHttpActionResult AutoListPerson(string BrnId, string CompId, string id, string Prefix, string GrpId, string Uid, string Mode)
        {
            return Ok(Manager.PersonsManager.AutoListPerson(BrnId, CompId, id, Prefix, GrpId, Uid, Mode));
        }
        [HttpPost]
        [Route("Insert")]
        public void CreatePerson(Persons persons)
        {
            Manager.PersonsManager.CreatePerson(persons);
        }
    }
}
