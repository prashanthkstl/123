﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;

using System.Data;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;


using Dapper;
namespace ChitCare.Services.Controllers
{
    [RoutePrefix("User")]
    public class UserApiController : ChitCareApiController
    {
        [HttpPost]
        [Route("UserValidate")]
        public IHttpActionResult ValiDateUser(Login Login)
        {
          
            return Ok(Manager.UserLoginManager.ValiDateUser(Login));
        }
        [HttpGet]
        [Route("GetUser")]
        public IHttpActionResult GetUser(int id)
        {
            return Ok(Manager.UserLoginManager.GetUser(id));
        }
        [HttpPost]
        [Route("IPAddressCheck")]
        public IHttpActionResult IPAddressCheck(IPCheck ipCheck)
        {

            return Ok(Manager.UserLoginManager.ChekIpAddress(ipCheck));
        }
    }
       
}
