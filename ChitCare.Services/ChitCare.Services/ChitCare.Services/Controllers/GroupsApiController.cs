﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.Services.Controllers
{
    [RoutePrefix("Groups")]
    public class GroupsApiController : ChitCareApiController
    {
        [Route("GroupEntry")]
        public void GroupEntry(Groups groups)
        {
            Manager.GroupManager.GroupEntry(groups);
        }

        [HttpGet]
        [Route("GroupView")]

        public IHttpActionResult GroupView(int Branch, string GroupName,string ChitSeries, DateTime FromDate, DateTime ToDate)
        {
            return Ok(Manager.GroupManager.GroupView(Branch, GroupName,ChitSeries, FromDate, ToDate));
        }

        [HttpGet]

        [Route("GroupEdit")]
        public IHttpActionResult GroupEdit(int GroupId)
        {
            return Ok(Manager.GroupManager.GroupEdit(GroupId));
        }

        [HttpGet]

        [Route("BranchList")]

        public IHttpActionResult BranchList()
        {
            return Ok(Manager.GroupManager.BranchList());
        }
    }
}
