﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;

namespace ChitCare.Services.Controllers
{
    [RoutePrefix("Finyear")]
    public class FinyearApiController : ChitCareApiController
    {
        [HttpGet]
        [Route("FinyearList")]
        public IHttpActionResult GetFinyear(int id)
        {
          return  Ok(Manager.FinyearManager.GetFinyear(id));
        }
        [HttpGet]
        [Route("CompanyList")]
        public IHttpActionResult GetCompany(int id)
        {
            return Ok(Manager.FinyearManager.GetCompany(id));
        }
    }
}
