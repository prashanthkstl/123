﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Common.IoC;
using ChitCare.BussinessManagers;
namespace ChitCare.Services.Controllers
{
    public class ChitCareApiController : ApiController
    {
         [SimpleIoCPropertyInject]
        public IManager Manager { get; set; }
    }
}
