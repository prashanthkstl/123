﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using Dapper;
using System.Data;
namespace ChitCare.BussinessManagers
{
   public class UserLoginManager :BaseManager, IUserLoginManager
    {
        public UserLoginManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        public Login ValiDateUser(Login Login)
        {
            var p = new DynamicParameters();

            p.Add("@UserCode", Login.UserCode);
            p.Add("@out", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("@UserId", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("@Salutation", dbType: DbType.String, direction: ParameterDirection.Output,size:10);
            p.Add("@Uname", dbType: DbType.String, direction: ParameterDirection.Output,size:50);

            DynamicRepository.FindParameters<object>("Users_LoginCheck", p);
            Login.UserId = p.Get<int>("@UserId");
            Login.Uname = p.Get<string>("@Uname");
            Login.Salutation = p.Get<string>("@Salutation");
            Login.Result = p.Get<int>("@out");
            return Login;
        }
        public IPCheck ChekIpAddress(IPCheck ipCheck)
        {
            var p = new DynamicParameters();

          
            p.Add("@UserId",ipCheck.UserId);
            p.Add("@IPAddress", ipCheck.IPAddress);
            p.Add("@Allow", dbType: DbType.Boolean, direction: ParameterDirection.Output);

            DynamicRepository.FindParameters<object>("Users_ChkIPs", p);
            ipCheck.Allow = p.Get<Boolean>("@Allow");
            return ipCheck;
        }
        public User GetUser(int id)
        {
            return DynamicRepository.FindUser<User>("Users_Retrieve", id);
        }
    }
}
