﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.BussinessManagers
{
   public interface IPlacesManager
    {
            IUnitOfWork UnitOfWork { get; set; }
            IDynamicRepository DynamicRepository { get; set; }

            void CountryInsert(Countries countries);
            void CountryUpdate(Countries countries);
            void DeleteCountry(Countries countries);
            List<Countries> GetCountries();
            Countries CountryEdit(int CountryId);


            void StateInsert(States countries);
            //void StateUpdate(States countries);
            void DeleteState(States countries);
            List<States> GetStates();
            List<States> StateView(int CountryId);
            States StateEdit(int StateId);


            void CitiesInsert(Cities cities);
            void CitiesUpdate(Cities cities);
            void DeleteCity(Cities cities);
            List<Cities> GetCities();
            List<Cities> CityView(int CityDistrictId, string CityName);
            Cities CityEdit(int CityId);


            void DistInsert(Districts dist);
            void DistUpdate(Districts dist);
            void DeleteDist(Districts dist);
            List<Districts> GetDistricts();
            List<Districts> DistrictView(int StateId);
            Districts DistrictEdit(int DistId);


            void RouteInsert(Routes route);
            List<Routes> RouteView(int BranchId);
            Routes RouteEdit(int RouteID);

            void AreaEntry(Areas area);
            List<Areas> AreaView(int RouteID);
            Areas AreaEdit(int AreaID);
            List<Routes> GetRoutes();

    }
}
