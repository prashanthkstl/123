﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.BussinessManagers
{
  public  interface IUserLoginManager
    {
        IUnitOfWork UnitOfWork { get; set; }
        IDynamicRepository DynamicRepository { get; set; }
        Login ValiDateUser(Login Login);
        User GetUser(int id);
        IPCheck ChekIpAddress(IPCheck ipCheck);
    }
}
