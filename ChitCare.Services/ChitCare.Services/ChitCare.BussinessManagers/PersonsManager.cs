﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;
using Dapper;
namespace ChitCare.BussinessManagers
{
    public class PersonsManager : BaseManager, IPersonsManager
    {

        public PersonsManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        public List<PersonsAutoList> AutoListPerson(string BrnId, string CompId, string id, string Prefix, string GrpId, string Uid, string Mode)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@BrnId", Convert.ToInt16(BrnId));
            p.Add("@CompId", Convert.ToInt32(CompId));
            p.Add("@id", Convert.ToInt32(id));
            p.Add("@Prefix", Prefix);
            p.Add("@GrpId", Convert.ToInt32(GrpId));
            p.Add("@Uid", Convert.ToInt32(Uid));
            p.Add("@Mode", Convert.ToInt16(Mode));


            return DynamicRepository.All<PersonsAutoList>("Persons_AutoFill", p);

        }
        public void CreatePerson(Persons persons)
        {
            List<PersonsAddress> Alist = persons.Address;
            List<PersonOccViewModel> OccList = new List<PersonOccViewModel>();
            OccList.Add(persons.personOccViewModel);
            DataTable dtAddress = ConvertToDataTable<PersonsAddress>(Alist);
            DataTable Occupation = ConvertToDataTable<PersonOccViewModel>(OccList);
            DynamicParameters p = new DynamicParameters();
            #region Persons ParameterList
            p.Add("@PersonId", persons.PersonId);
            p.Add("@BranchId", persons.BranchId);
            p.Add("@RegistrationDate", persons.RegistrationDate, dbType: DbType.DateTime);
            p.Add("@PersonCode", persons.PersonCode);
            p.Add("@Password", persons.Password,dbType:DbType.Binary);
            p.Add("@SaltValue", persons.SaltValue, dbType: DbType.Binary);
            p.Add("@Honor", persons.Honor);
            p.Add("@PersonName", persons.PersonName);
            p.Add("@SurName", persons.SurName);
            p.Add("@CofGaurdianName", persons.CofGaurdianName);
            p.Add("@Gender", persons.Gender);
            p.Add("@DOB", persons.DOB, dbType: DbType.DateTime);
            p.Add("@PanNo", persons.PanNo);
            p.Add("@Occupation", persons.Occupation);
            p.Add("@Signature", persons.Signature);
            p.Add("@PersonPhoto", persons.PersonPhoto);
            p.Add("@GuardianName", persons.GuardianName);
            p.Add("@GuardianRelation", persons.GuardianRelation);
            p.Add("@InroducerName", persons.InroducerName);
            p.Add("@IsMember", persons.IsMember);
            p.Add("@IsAgent", persons.IsAgent);
            p.Add("@IsStaff", persons.IsStaff);
            p.Add("@IsGaruntor", persons.IsGaruntor);
            p.Add("@GSTNo", persons.GSTNo);
            p.Add("@AdhaarCardNo", persons.AdhaarCardNo);
            p.Add("@Email", persons.Email);
            p.Add("@PersonSatus", persons.PersonSatus);
            p.Add("@MainAgentId", persons.MainAgentId);
            p.Add("@GPS_Location", persons.GPS_Location);
            p.Add("@CollectionLimit", persons.CollectionLimit);
            p.Add("@LastLogin", persons.LastLogin, dbType: DbType.DateTime);
            p.Add("@CreatedBy", persons.CreatedBy);
            p.Add("@CreatedDate", persons.CreatedDate, dbType: DbType.DateTime);
            p.Add("@ModifiedBy", persons.ModifiedBy);
            p.Add("@ModifiedDate", persons.ModifiedDate, dbType: DbType.DateTime);
            
            p.Add("@CellNo", persons.CellNo);
            p.Add("@AddDetails", dtAddress.AsTableValuedParameter("TVP_AddressDetails"));
            p.Add("@PersonsOccDetails", Occupation.AsTableValuedParameter("TVP_PersonsOccDetails"));
            #endregion

            DynamicRepository.FindParameters<Persons>("PersonsInsertOrUpdate", p);

        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

    }
}
