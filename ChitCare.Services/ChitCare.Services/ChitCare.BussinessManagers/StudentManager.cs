﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using Dapper;
namespace ChitCare.BussinessManagers
{
   public class StudentManager:BaseManager,IStudentManager
    {
        public StudentManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        public void Save(Student student)
        {
            DynamicParameters p = new DynamicParameters();
            p.AddDynamicParams(student);
            DynamicRepository.FindParameters<Student>("sp_StudInsert", p);
        }
        public void Update(Student student)
        {

            DynamicParameters p = new DynamicParameters();
            p.AddDynamicParams(student);
            DynamicRepository.FindParameters<Student>("sp_StudUpdate", p);
        }
       public void DeleteMultiple(Student student)
        {

            DynamicRepository.Delete(student.StudId, "Student", "StudID");
        }
        public List<Student> GetStudent()
        {
            return DynamicRepository.All<Student>("sp_StudentRetrieve", null);
        }

    }
}
