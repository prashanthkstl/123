﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using Dapper;
using ChitCare.Services.DAL.Reposotories;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.BussinessManagers
{
   public class GroupsManager:BaseManager,IGroupsManager
    {

        public GroupsManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        public void GroupEntry(Groups group)
        {
             List<GroupsSuriety> gSurietyList = new List<GroupsSuriety>();
             gSurietyList.Add(group.groupSuriety);
             DataTable dtGroupSuriety= ConvertToDataTable(gSurietyList);
            DynamicParameters p = new DynamicParameters();
            p.Add("@CeilingPer", group.CeilingPer, dbType: DbType.Int32);
            p.Add("@ReceiptPosting", group.ReceiptPosting, dbType: DbType.Byte);
            p.Add("@PaymentPosting", group.PaymentPosting, dbType: DbType.Byte);
            p.Add("@PSPenalty", group.PSPenalty, dbType: DbType.Int32);
            p.Add("@NPSPenalty", group.NPSPenalty, dbType: DbType.Int32);
            p.Add("@CompanyCommission", group.CompanyCommission, dbType: DbType.Int32);
            p.Add("@CompanyAuctionNo", group.CompanyAuctionNo, dbType: DbType.Int32);
            p.Add("@CompanyChit", group.CompanyChit, dbType: DbType.Int32);
            p.Add("@StratDate", group.StratDate, dbType: DbType.DateTime);
            p.Add("@EnrollmentFee", group.EnrollmentFee, dbType: DbType.Int32);
            p.Add("@RegistrationAt", group.RegistrationAt, dbType: DbType.String);
            p.Add("@TerminationDate", group.TerminationDate, dbType: DbType.DateTime);
            p.Add("@CommenseDate", group.CommenseDate, dbType: DbType.DateTime);
            p.Add("@ByeLawDate", group.ByeLawDate, dbType: DbType.DateTime);
            p.Add("@ByeLawNo", group.ByeLawNo, dbType: DbType.String);
            p.Add("@PSODate", group.PSODate, dbType: DbType.DateTime);
            p.Add("@PSONo", group.PSONo, dbType: DbType.String);
            p.Add("@GroupSeries", group.GroupSeries, dbType: DbType.String);
            p.Add("@Instalments", group.Instalments, dbType: DbType.Int32);
            p.Add("@ChitType", group.ChitType, dbType: DbType.Int32);
            p.Add("@AuctType", group.AuctType, dbType: DbType.Int32);
            p.Add("@ChitAmount", group.ChitAmount, dbType: DbType.Int32);
            p.Add("@BranchId", group.BranchId, dbType: DbType.Int32);
            p.Add("@GroupName", group.GroupName, dbType: DbType.String);
            p.Add("@GroupId", group.GroupId, dbType: DbType.Int32);
            p.Add("@AuctionPosting", group.AuctionPosting, dbType: DbType.Byte);
            p.Add("@BTPosting", group.BTPosting, dbType: DbType.Byte);
            p.Add("@DividendRound", group.DividendRound, dbType: DbType.Byte);
            p.Add("@Dividend", group.Dividend, dbType: DbType.Int32);
            p.Add("@NoAuctions", group.NoAuctions, dbType: DbType.Int32);
            p.Add("@InstalmentAmt", group.InstalmentAmt, dbType: DbType.Int32);
            p.Add("@AuctTimeFrom", group.AuctTimeFrom, dbType: DbType.String);
            p.Add("@AuctTimeTo", group.AuctTimeTo, dbType: DbType.String);
            p.Add("@AuctDay", group.AuctDay, dbType: DbType.String);
            p.Add("@WeekNo", group.WeekNo, dbType: DbType.Int32);
            p.Add("@WeekNo1", group.WeekNo1, dbType: DbType.Int32);
            p.Add("@NoOfFrations", group.NoOfFrations, dbType: DbType.Int32);
            p.Add("@FractionInst", group.FractionInst, dbType: DbType.Int32);
            p.Add("@FractionAmount", group.FractionAmount, dbType: DbType.Int32);
            p.Add("@IsGroupClose", group.IsGroupClose, dbType: DbType.Byte);
            p.Add("@CreatedBy", group.CreatedBy, dbType: DbType.Int32);
            p.Add("@ModifiedBy", group.ModifiedBy, dbType: DbType.Int32);
            p.Add("@Remarks", group.Remarks, dbType: DbType.String);
            p.Add("@TVP_GroupSurety", dtGroupSuriety.AsTableValuedParameter("TVP_GroupSurety"));

            DynamicRepository.FindParameters<Groups>("ChitGroupInsertOrUpdate", p);
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        public List<GroupsList> GroupView(int Branch, string GroupName,string ChitSeries,DateTime FromDate,DateTime ToDate)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@BranchId", Convert.ToInt16(Branch));
            p.Add("@GroupName", GroupName);
            p.Add("@Series", ChitSeries);
            p.Add("@FromDate", FromDate);
            p.Add("@ToDate", ToDate);
            return DynamicRepository.All<GroupsList>("GroupsList", p);
        }

        public Groups GroupEdit(int GroupID)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@GroupId", GroupID);
            Groups group= DynamicRepository.FindBy<Groups>("Group_Retrieve", p);
            group.groupSuriety= DynamicRepository.FindBy<GroupsSuriety>("GroupSuriety_Retrieve", p);
            return group;
        }

        public List<Branches> BranchList()
        {
            return DynamicRepository.All<Branches>("BranchList", null);
        }
    }
}
