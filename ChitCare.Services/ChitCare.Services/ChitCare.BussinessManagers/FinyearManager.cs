﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using Dapper;
namespace ChitCare.BussinessManagers
{
    public class FinyearManager : BaseManager, IFinyearManager
    {
        public FinyearManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        public List<Finyear> GetFinyear(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@compId", id);
            return DynamicRepository.All<Finyear>("FinYear_CmbFill",p);
        }
        public List<Company> GetCompany(int id)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@UserId", id);
            return DynamicRepository.All<Company>("CmbFill_CompaniesAccess", p);
        }
    }
}

