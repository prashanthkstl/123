﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.Controllers;

namespace ChitCare.Services.Areas.Master.Controllers
{
    [RoutePrefix("Common")]
    public class CommonApiController : ChitCareApiController
    {
        [HttpGet]
        [Route("Branches")]
      public IHttpActionResult GetBranches()
        {
            return Ok(Manager.CommonManager.GetBranches());
        }
        [HttpGet]
        [Route("AutoListGroups")]
        public IHttpActionResult AutoListGroups(string Prefix,int Id)
        {
            return Ok(Manager.CommonManager.AutoListGroups(Prefix, Id));
        }
       
    }
}
