﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.Services.Controllers
{
   
    [RoutePrefix("Persons")]
    public class PersonsApiController : ChitCareApiController
    {
        [HttpGet]
        [Route("AutoList")]
        public IHttpActionResult AutoListPerson(string BranchId, string CompanyId, string Id, string Prefix, string GroupId, string UserId, string Mode)
        {
            return Ok(Manager.PersonsManager.AutoListPerson(BranchId, CompanyId, Id, Prefix, GroupId, UserId, Mode));
        }
        [HttpPost]
        [Route("Insert")]
        public void CreatePerson(Persons persons)
        {
            Manager.PersonsManager.CreatePerson(persons);
        }
        [HttpGet]
        [Route("CityAutoList")]
        public IHttpActionResult CityAutoList(string Prefix)
        {
            return Ok(Manager.PersonsManager.CityAutoList(Prefix));
        }
        [HttpGet]
        [Route("GetPersons")]
        public IHttpActionResult GetPersonData(string BranchId, string Prefix, string Fdate, string ToDate,int ListType)
        {
            return Ok(Manager.PersonsManager.GetPersons(BranchId,Prefix,Fdate,ToDate,ListType));
        }
        [HttpGet]
        [Route("EditPerson")]
        public IHttpActionResult EditPersons(int Id)
        {
            return Ok(Manager.PersonsManager.EditPerson(Id));
        }
        [HttpGet]
        [Route("GetPlacesByCityId")]
        public IHttpActionResult GetPlacesByCityId(int Id)
        {
            return Ok(Manager.PersonsManager.GetPlacesByCityId(Id));
        }
        [HttpGet]
        [Route("GetPlacesByPersonId")]
        public IHttpActionResult GetPlacesByPersonId(int Id)
        {
            return Ok(Manager.PersonsManager.GetPlacesByPersonId(Id));
        }
    }
}
