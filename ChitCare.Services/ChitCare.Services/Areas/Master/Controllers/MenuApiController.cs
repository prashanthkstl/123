﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.Services.Controllers
{
    [RoutePrefix("Menu")]
    public class MenuApiController : ChitCareApiController
    {
        [HttpGet]
        [Route("MainMenu")]
        public IHttpActionResult GetMainMenu()
        {
            return Ok(Manager.ModuleScreenManager.GetMenu());
        }
    }
}
