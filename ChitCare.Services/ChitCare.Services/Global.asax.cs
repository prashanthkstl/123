﻿using Common.IoC;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System.Web.Routing;
using ChitCare.BussinessManagers;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;


using System.Web.Http;

namespace ChitCare.Services
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RegisterSimpleIoC();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        protected void RegisterSimpleIoC()
        {
            Container container = new Container();
            container.Options.AutowirePropertiesWithAttribute<SimpleIoCPropertyInjectAttribute>();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            // This is an extension method from the integration package.
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            #region Connection and Unit of work registry
            container.Register(typeof(IConnectionFactory), typeof(ConnectionFactory));
            container.Register(typeof(IUnitOfWork), typeof(UnitOfWork));
            #endregion

            #region Managers registry
            container.Register(typeof(IManager), typeof(Manager));
            container.Register(typeof(IFinyearManager), typeof(FinyearManager));
            container.Register(typeof(IUserLoginManager), typeof(UserLoginManager));
            container.Register(typeof(IPersonsManager), typeof(PersonsManager));
            container.Register(typeof(IPlacesManager), typeof(PlacesManager));
            container.Register(typeof(IModuleScreenManager), typeof(ModuleScreenManager));
            container.Register(typeof(IGroupsManager), typeof(GroupsManager));
            #endregion

            #region repostories registry

            container.Register(typeof(IDynamicRepository), typeof(DynamicRepositoryBase));
            #endregion

            //Web Api dependency resolver setting.
            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}

