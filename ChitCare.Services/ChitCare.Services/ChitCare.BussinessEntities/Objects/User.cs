﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class User
    {

        public bool IsDirector { get; set; }

        public bool UserAccessType { get; set; }

        public string IPAddresses { get; set; }


        public string AccessCompIds { get; set; }

        public int UserId { get; set; }

        public string UserCode { get; set; }

        public byte[] Password { get; set; }

        public byte[] SaltValue { get; set; }

        public short RoleId { get; set; }

        public string Salutation { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool Status { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string RoleName { get; set; }

        public string AccessBranchIds { get; set; }

        public bool AccessType { get; set; }

        public string LastLogin { get; set; }
    }
}
