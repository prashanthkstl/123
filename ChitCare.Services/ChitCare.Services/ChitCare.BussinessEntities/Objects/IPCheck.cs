﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class IPCheck
    {
        public string UserId { get; set; }

        public string IPAddress { get; set; }
        public bool Allow { get; set; }
    }
}
