﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class PersonOccDetails
    {


        public int OccPersonId { get; set; }

        public int OccupationType { get; set; }

        public string CompanyName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string EmployeeId { get; set; }
        public DateTime EmployeeJoinDate { get; set; }
        public DateTime Retire_Date { get; set; }
        public decimal Salary { get; set; }
        public decimal HRA { get; set; }
        public decimal DA { get; set; }
        public decimal OtherAllowances { get; set; }
        public decimal Dedutions { get; set; }
        public decimal Net_Salary { get; set; }
        public string TAN { get; set; }
        public string FirmName { get; set; }
        public decimal CapitalInvestment { get; set; }
        public decimal  Income { get; set; }
        public string NOB { get; set; }


    }
}
