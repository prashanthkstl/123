﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class Groups
    {
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public int BranchId { get; set; }
        public double ChitAmount { get; set; }
        public int AuctType { get; set; }
        public int ChitType { get; set; }
        public int Instalments { get; set; }
        public string GroupSeries { get; set; }
        public string PSONo { get; set; }
        public DateTime PSODate { get; set; }
        public string ByeLawNo { get; set; }
        public DateTime ByeLawDate { get; set; }
        public DateTime CommenseDate { get; set; }
        public DateTime TerminationDate { get; set; }
        public string RegistrationAt { get; set; }
        public double EnrollmentFee { get; set; }
        public DateTime StratDate { get; set; }
        public int CompanyChit { get; set; }
        public int CompanyAuctionNo { get; set; }
        public double CompanyCommission { get; set; }
        public double NPSPenalty { get; set; }
        public double PSPenalty { get; set; }
        public double CeilingPer { get; set; }
        public byte ReceiptPosting { get; set; }
        public byte PaymentPosting { get; set; }
        public byte AuctionPosting { get; set; }
        public byte BTPosting { get; set; }
        public byte DividendRound { get; set; }
        public int Dividend { get; set; }
        public int NoAuctions { get; set; }
        public double InstalmentAmt { get; set; }
        public string AuctTimeFrom { get; set; }
        public string AuctTimeTo { get; set; }
        public string AuctDay { get; set; }
        public int WeekNo { get; set; }
        public int WeekNo1 { get; set; }
        public int NoOfFrations { get; set; }
        public double FractionInst { get; set; }
        public double FractionAmount { get; set; }
        public byte IsGroupClose { get; set; }
        public int CreatedBy { get; set; }

        public int ModifiedBy { get; set; }

        public string Remarks { get; set; }
        public GroupsSuriety groupSuriety { get; set; }
    }

    public class GroupsSuriety
    {

        public int GrpSuretyType { get; set; }
        
        public string FDRNo { get; set; }
        public DateTime FDRDate { get; set; }
        public int FDRNoofMonths { get; set; }
        public int FDRType { get; set; }
        public double FDRAmount { get; set; }
        public double FDRInterest { get; set; }
        public DateTime FDRMaturityDate { get; set; }
        public double FDRMaturityAmount { get; set; }
        public string FDRBank { get; set; }
        public string FDRBankBranch { get; set; }
        public string MortgageDesc { get; set; }
        public double MortgageValue { get; set; }
        public DateTime MortgageDate { get; set; }
    }
}
