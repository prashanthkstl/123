﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class States
    {
        public int StateId { get; set; }
        public int StateCountryId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }
    }
    
}
