﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class Modules
    {
        public int ModuleId { get; set; }
        public string ModuleName { get; set; }
    }
}
