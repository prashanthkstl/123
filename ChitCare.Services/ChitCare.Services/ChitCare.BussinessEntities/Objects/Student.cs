﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class Student
    {
        public int StudId { get; set; }
        public string name { get; set; }
        public string Gender { get; set; }
        public int Maths { get; set; }
        public int Physics { get; set; }
        public int Chemistry { get; set; }
        public int Total { get; set; }
    }
}
