﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class Cities
    {
        public int CityId { get; set; }

        public int CountryId { get; set; }

        public int DistrictId { get; set; }

        public int StateId { get; set; }
        public string CityName { get; set; }
        public string PinCode { get; set; }
        public int CityDistrictId { get; set; }
        
    }
}
