﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class PersonDetails:AddressDetails
    {


        public int BranchId { get; set; }
        public string PresonCode { get; set; }
        public byte[] Password { get; set; }
        public byte[] SaltValue { get; set; }
        public string Honor { get; set; }
        public string PersonName { get; set; }
        public string SurName { get; set; }
        public string CofGaurdianName { get; set; }
        public byte Gender { get; set; }
        public DateTime DOB { get; set; }
        public string PanNo { get; set; }
        public string Occupation { get; set; }
        public string Signature { get; set; }
        public string PersonPhoto { get; set; }
        public byte IsMember { get; set; }
        public byte IsAgent { get; set; }
        public byte IsStaff { get; set; }
        public byte IsGaruntor { get; set; }
        public string GSTNo { get; set; }
        public string AdhaarCardNo { get; set; }
        public string PersonSatus { get; set; }
        public int MainAgentId { get; set; }
        public string GPS_Location { get; set; }
        public int CollectionLimit { get; set; }
        public DateTime LastLogin { get; set; }
        public string GuardianName { get; set; }
        public string GuardianRelation { get; set; }
        public string InroducerName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CellNo { get; set; }
        public int output { get; set; }
    }







    public class AddressDetails
    {
        public int AddPersonId { get; set; }
        public int AddressType { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public string PinCode { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}
