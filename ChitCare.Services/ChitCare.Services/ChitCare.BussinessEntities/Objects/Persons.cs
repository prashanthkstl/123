﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class Persons : PersonRequest
    {
        public int PersonId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string PersonCode { get; set; }
        public byte[] Password { get; set; }
        public byte[] SaltValue { get; set; }
        public string Honor { get; set; }
        //public string PersonName
        public string SurName { get; set; }
        public string CofGaurdianName { get; set; }
        public bool Gender { get; set; }
        public DateTime DOB { get; set; }
        public string PanNo { get; set; }
        public int Occupation { get; set; }
        public string Signature { get; set; }
        public string PersonPhoto { get; set; }
        public string GuardianName { get; set; }
        public string GuardianRelation { get; set; }
        public string InroducerName { get; set; }
        public bool IsMember { get; set; }
        public bool IsAgent { get; set; }
        public bool IsStaff { get; set; }
        public bool IsGaruntor { get; set; }
        public string GSTNo { get; set; }
        public string AdhaarCardNo { get; set; }
        public string Email { get; set; }
        public bool PersonSatus { get; set; }
        public long MainAgentId { get; set; }
        public string GPS_Location { get; set; }
        public double CollectionLimit { get; set; }
        public DateTime LastLogin { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string CellNo { get; set; }
       
        public  List<PersonsAddress> Address { get; set; }
        public PersonOccViewModel personOccViewModel { get; set; }
    }
    public class PersonRequest
    {
        public int BranchId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string PersonName { get; set; }
        public int PersonType { get; set; }
        public int PageSize { get; set; }
        public int PageNo { get; set; }

    }
    public class PersonsAddress
    {
        public int AddressType { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public string PinCode { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Fax { get; set; }
       
    }
    public class PersonOccViewModel
    {
        public int OccupationType { get; set; }

        public string CompanyName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string EmployeeId { get; set; }
        public DateTime EmployeeJoinDate { get; set; }
        public DateTime RetireDate { get; set; }
        public decimal Salary { get; set; }
        public decimal HRA { get; set; }
        public decimal DA { get; set; }
        public decimal OtherAllowances { get; set; }
        public decimal Dedutions { get; set; }
        public decimal NetSalary { get; set; }
        public string TAN { get; set; }
        public string FirmName { get; set; }
        public decimal Capital { get; set; }
        public decimal Income { get; set; }
        public string NOB { get; set; }
    }
}
