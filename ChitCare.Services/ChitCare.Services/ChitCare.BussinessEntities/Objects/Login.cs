﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class Login
    {
        public string UserCode { get; set; }

        public string Uname { get; set; }

        public int Result { get; set; }
        public string Salutation { get; set; }
        public int UserId { get; set; }
    }
}
