﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class Districts
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public int DistrictStateId { get; set; }

        public int CountryId { get; set; }

        public int StateId { get; set; }
      
    }
}
