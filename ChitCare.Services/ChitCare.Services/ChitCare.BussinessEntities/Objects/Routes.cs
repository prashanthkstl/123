﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class Routes
    {
        public int RouteID { get; set; }

        public string RouteName { get; set; }

        public int RouteBranchId { get; set; }

        public int BranchId { get; set; }
    }
}
