﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class SubModules
    {
        public int SubModuleId { get; set; }
        public int ModuleId { get; set; }
        public string SubModule { get; set; }
        public string ModuleImage { get; set; }
        public int DisplayOrder { get; set; }
    }
}
