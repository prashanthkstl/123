﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class Enrollments
    {
        //public int TicketNo { get; set; }
        //public int TicketName { get; set; }
        public long EnrollmentId { get; set; }
        public DateTime EnrollmentDate { get; set; }
        public int BranchId { get; set; }
        public int GroupId { get; set; }
        public int ChitNo { get; set; }
        public long PersonId { get; set; }
        public int PaymentType { get; set; }
        public byte SF_Setup { get; set; }
        public byte Own_Chit { get; set; }
        public string Rep_By { get; set; }
        public string Nomineename { get; set; }
        public string NomineeRelation { get; set; }
        public int NomineeAge { get; set; }
        public long AgentId { get; set; }
        public bool Eligibility { get; set; }
        public int IC_Print { get; set; }
        public int IC_Type { get; set; }
        public long StaffID { get; set; }
        public byte EnrolPosition { get; set; }
        public int CancelId { get; set; }
        public byte BadDebitor { get; set; }
        public int AgrmtReceived { get; set; }
        public DateTime AgrmtDate { get; set; }
        public bool IsPBIssued { get; set; }
        public DateTime PBIssusedDate { get; set; }
        public string NomineeAddress1 { get; set; }
        public string NomineeAddress2 { get; set; }
        public string NomineeAddress3 { get; set; }
        public int NomineeCity { get; set; }
        public int NomineeDistrict { get; set; }
        public int NomineeState { get; set; }
        public int NomineeCountry { get; set; }
        public string NomineePincode { get; set; }
        public string NomineePhoneNo { get; set; }
        public double Payable { get; set; }
        public double Paid { get; set; }
        public double Balance { get; set; }
        public bool AddressCorr { get; set; }
        public DateTime SFDate { get; set; }
        public DateTime PSDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int AreaId { get; set; }
        public int OutVisible { get; set; }
        public double IntencivePercentage { get; set; }
        public bool IntenciveAllow { get; set; }
        public string SF { get; set; }
        public string SuitNo { get; set; }
        public byte IsSecondAccount { get; set; }
        public DateTime SecondACDate { get; set; }
        public int IntroducerInc { get; set; }
        public byte UnderRecovery { get; set; }
        public long zoneid { get; set; }
        public long DirectorId { get; set; }
        public int Intimate_Type { get; set; }
        public long cusomerid { get; set; }
        public string Passbookno { get; set; }
        public string StateName { get; set; }
        public string countryName { get; set; }
        public string CityName { get; set; }
        public string DistrictName { get; set; }
        public string GroupName { get; set; }
        public string SubscriberName { get; set; }
        public string BussinessAgent { get; set; }
        public string CollectionAgent { get; set; }
        public int Instalments { get; set; }
    }
    public class EnrollmentList
    {
        public int EnrollmentId { get; set; }
        public string GroupName { get; set; }
        public int ChitNo { get; set; }
        public string Subscriber { get; set; }
        public string PaymentType { get; set; }
        public DateTime EnrollDate { get; set; }
        public string CollectionAgent { get; set; }
        public string BussinessAgent { get; set; }
        public string PhoneNo { get; set; }
        public string UserName { get; set; }

    }
}
