﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class Areas
    {
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public int AreaRouteId { get; set; }

        public int AreaBranchId { get; set; }

        public int RouteID { get; set; }

        public string RouteName { get; set; }

        public int BranchId { get; set; }
    }
}
