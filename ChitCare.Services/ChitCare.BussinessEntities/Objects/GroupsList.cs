﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
    public class GroupsList
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }

        public int Instalments { get; set; }

        public DateTime CommenseDate { get; set; }

        public string GroupSeries { get; set; }

        public DateTime TerminationDate { get; set; }
        public string ByeLawNo { get; set; }
        public DateTime ByeLawDate { get; set; }
        public int ChitAmount { get; set; }
    }
}
