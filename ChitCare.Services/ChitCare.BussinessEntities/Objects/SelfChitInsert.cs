﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class SelfChitInsert
    {
        public int SelfID { get; set; }
        public int ListNo { get; set; }
        public long SPersonId { get; set; }
        public long PersonId { get; set; }
        public long EnrollId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public byte IsGroupWise { get; set; }
    }
}
