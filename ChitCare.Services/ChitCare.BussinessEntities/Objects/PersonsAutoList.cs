﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class PersonsAutoList
    {

        public string Name { get; set; }
        public string PrId { get; set; }
        public string ChitNo { get; set; }

        public string EnlId { get; set; }
        public string GrpId { get; set; }
        public string GrpName { get; set; }
        public string SuitStatus { get; set; }
        public string DecreeId { get; set; }
    }
}
