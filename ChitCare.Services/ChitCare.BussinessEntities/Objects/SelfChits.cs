﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
   public class SelfChits
    {
        public SelfChitsAddress selfChitAddress { get; set; }
        public List<SelfChitList> selfChitList { get; set; }
    }
    public class SelfChitsAddress
    {
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PersonName { get; set; }
        public string CoGuardianName { get; set; }
        public int ListNo { get; set; }
        public string CityName { get; set; }
    }
    public class SelfChitList
    {
        public string Name { get; set; }
        public string SODO { get; set; }
        public string Place { get; set; }
        public string Phone { get; set; }
        public string ChitRef { get; set; }
        public int EnrollmentId { get; set; }
        public int GroupId { get; set; }
        public int SPersonId { get; set; }
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public int ChitNo { get; set; }
        public int ListNo { get; set; }
        public string CoGuardianName { get; set; }
        public string Names { get; set; }
        public string NoOfPersons { get; set; }
    }
}
