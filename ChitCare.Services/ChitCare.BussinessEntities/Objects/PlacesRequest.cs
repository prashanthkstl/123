﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.BussinessEntities.Objects
{
  public  class PlacesRequest: CityAList
    {
        public int CountryId { get; set; }
        public string countryName { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string PinCode { get; set; }
        public string Address { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string PersonName { get; set; }
        public string CoGuardianName { get; set; }
        public int ListNo { get; set; }
    }

    public class CityAList
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
    }
    public class PersonsData
    {
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public string CGuardian { get; set; }

        public string CellNo { get; set; }

        public string Activity { get; set; }
    }
}
