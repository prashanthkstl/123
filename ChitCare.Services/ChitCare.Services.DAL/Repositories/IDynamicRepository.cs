﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Dapper;
namespace ChitCare.Services.DAL.Reposotories
{
   public interface IDynamicRepository
    {
        IUnitOfWork UnitOfWork { get; set; }
        int Add<T>(string uspName, T item);
        int AddOrUpdateDynamic(string uspName, dynamic entity);
      
        List<T> All<T>(string uspName, object param);
        
        void Delete(int id, string tblName, string idName);
        int DeleteMultiple(string uspName, string ids);
        void Delete<T>(T entity);
        T Find<T>(string uspName, int id);
        T FindBy<T>(string uspName, object entityParam);
        object FindParameters<T>(string uspName, object entity);
        T FindByName<T>(string name);
        int Update<T>(string uspName, T entity);
        void BulkSave(DataTable item, string[] param);
        T FindUser<T>(string uspName, int id);


    }
}
