﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using ChitCare.BussinessEntities.Objects;

namespace ChitCare.BussinessManagers
{
    public interface IMasterManager
    {
        IUnitOfWork UnitOfWork { get; set; }
        IDynamicRepository DynamicRepository { get; set; }
        List<Enrollments> GetVacantChits(int GroupId);
        void EnrollmentEntry(Enrollments enrollments);
        List<EnrollmentList> GetEnrollmentList(string BranchId, string Prefix, string Fdate, string ToDate, int GroupId);
        Enrollments EditEnrollment(int EnrollmentId);
        void ActivateOrDeactivatePerson(int Type, int PersonId);
        SelfChitList GetPersonByGroupId(int GroupId, int ChitNo);
        SelfChits GetSelfChitsPerson(int PersonId);
        List<SelfChitList> GetNewSelfChitDetails(int PersonId, int GroupId, int ChitNo);
        object CheckExist(int SPersonId, int PersonId,int EnrollId);
        object GetListNo(int BranchId);
        void SelfChitsEntry(List<SelfChitInsert> selfChitinsert);
    }
}
