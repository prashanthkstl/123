﻿using System.Collections.Generic;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.BussinessManagers
{
   public interface IFinyearManager
    {
        IUnitOfWork UnitOfWork { get; set; }
        IDynamicRepository DynamicRepository { get; set; }
        List<Finyear> GetFinyear(int id);
        List<Company> GetCompany(int id);
       
    }
}
