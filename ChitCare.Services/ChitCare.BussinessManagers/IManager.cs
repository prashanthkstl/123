﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
namespace ChitCare.BussinessManagers
{
   public interface IManager
    {
        IFinyearManager FinyearManager { get; set; }
        IUserLoginManager UserLoginManager { get; set; }
        IPersonsManager PersonsManager { get; set; }
        ///IStudentManager StudentManager { get; set; }
        IPlacesManager PlacesManager { get; set; }
        IModuleScreenManager ModuleScreenManager { get; set; }
        IGroupsManager GroupManager { get; set; }
    }
}
