﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using Dapper;
namespace ChitCare.BussinessManagers
{
   public class ModuleScreenManager : BaseManager,IModuleScreenManager
    {
        public ModuleScreenManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        public List<Menu> GetMenu()
        {
           
            return DynamicRepository.All<Menu>("sp_Menu",null);
        }
       
    }
}
