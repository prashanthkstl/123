﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.BussinessManagers
{
  public interface IGroupsManager
    {
        IUnitOfWork UnitOfWork { get; set; }
        IDynamicRepository DynamicRepository { get; set; }
        void GroupEntry(Groups groups);
        List<GroupsList>  GroupView(int Branch,string GroupName,string ChitSeries,DateTime FromDate,DateTime ToDate);

        Groups GroupEdit(int GroupID);

        List<Branches> BranchList();
    }
}
