﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using Dapper;
using System.Data;
using System.ComponentModel;
namespace ChitCare.BussinessManagers
{
    public class MasterManager : BaseManager, IMasterManager
    {
        public MasterManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        public List<Enrollments> GetVacantChits(int GroupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@Grp_Id", GroupId);
            return DynamicRepository.All<Enrollments>("Enrol_ChitNoVacentList", p);
        }
        public void EnrollmentEntry(Enrollments enrollments)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@EnrollmentId", enrollments.EnrollmentId);
            p.Add("@EnrollmentDate", enrollments.EnrollmentDate, dbType: DbType.DateTime);
            p.Add("@BranchId", enrollments.BranchId);
            p.Add("@GroupId", enrollments.GroupId);
            p.Add("@ChitNo", enrollments.ChitNo);
            p.Add("@PersonId", enrollments.PersonId);
            p.Add("@PaymentType", enrollments.PaymentType);
            p.Add("@SF_Setup", enrollments.SF_Setup);
            p.Add("@Own_Chit", enrollments.Own_Chit);
            p.Add("@Rep_By", enrollments.Rep_By);
            p.Add("@Nomineename", enrollments.Nomineename);
            p.Add("@NomineeRelation", enrollments.NomineeRelation);
            p.Add("@NomineeAge", enrollments.NomineeAge);
            p.Add("@AgentId", enrollments.AgentId);
            p.Add("@Eligibility", enrollments.Eligibility);
            p.Add("@IC_Print", enrollments.IC_Print);
            p.Add("@IC_Type", enrollments.IC_Type);
            p.Add("@StaffID", enrollments.StaffID);
            p.Add("@EnrolPosition", enrollments.EnrolPosition);
            p.Add("@CancelId", enrollments.CancelId);
            p.Add("@BadDebitor", enrollments.BadDebitor);
            p.Add("@AgrmtReceived", enrollments.AgrmtReceived);
            p.Add("@AgrmtDate", enrollments.AgrmtDate, dbType: DbType.DateTime);
            p.Add("@IsPBIssued", enrollments.IsPBIssued);
            p.Add("@PBIssusedDate", enrollments.PBIssusedDate, dbType: DbType.DateTime);
            p.Add("@NomineeAddress1", enrollments.NomineeAddress1);
            p.Add("@NomineeAddress2", enrollments.NomineeAddress2);
            p.Add("@NomineeAddress3", enrollments.NomineeAddress3);
            p.Add("@NomineeCity", enrollments.NomineeCity);
            p.Add("@NomineeDistrict", enrollments.NomineeDistrict);
            p.Add("@NomineeState", enrollments.NomineeState);
            p.Add("@NomineeCountry", enrollments.NomineeCountry);
            p.Add("@NomineePincode", enrollments.NomineePincode);
            p.Add("@NomineePhoneNo", enrollments.NomineePhoneNo);
            p.Add("@Payable", enrollments.Payable);
            p.Add("@Paid", enrollments.Paid);
            p.Add("@Balance", enrollments.Balance);
            p.Add("@AddressCorr", enrollments.AddressCorr);
            p.Add("@SFDate", enrollments.SFDate, dbType: DbType.DateTime);
            p.Add("@PSDate", enrollments.PSDate, dbType: DbType.DateTime);
            p.Add("@CreatedBy", enrollments.CreatedBy);
            p.Add("@CreatedDate", enrollments.CreatedDate, dbType: DbType.DateTime);
            p.Add("@ModifiedBy", enrollments.ModifiedBy);
            p.Add("@ModifiedDate", enrollments.ModifiedDate, dbType: DbType.DateTime);
            p.Add("@AreaId", enrollments.AreaId);
            p.Add("@OutVisible", enrollments.OutVisible);
            p.Add("@IntencivePercentage", enrollments.IntencivePercentage);
            p.Add("@IntenciveAllow", enrollments.IntenciveAllow);
            p.Add("@SF", enrollments.SF);
            p.Add("@SuitNo", enrollments.SuitNo);
            p.Add("@IsSecondAccount", enrollments.IsSecondAccount);
            p.Add("@SecondACDate", enrollments.SecondACDate, dbType: DbType.DateTime);
            p.Add("@IntroducerInc", enrollments.IntroducerInc);
            p.Add("@UnderRecovery", enrollments.UnderRecovery);
            p.Add("@zoneid", enrollments.zoneid);
            p.Add("@DirectorId", enrollments.DirectorId);
            p.Add("@Intimate_Type", enrollments.Intimate_Type);
            p.Add("@cusomerid", enrollments.cusomerid);
            p.Add("@Passbookno", enrollments.Passbookno);

            DynamicRepository.FindParameters<Persons>("EnrollmentInsertOrUpdate", p);
        }
        public List<EnrollmentList> GetEnrollmentList(string BranchId, string Prefix, string Fdate, string ToDate, int GroupId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@BranchId", Convert.ToInt32(BranchId));
            p.Add("@FromDate", Convert.ToDateTime(Fdate));
            p.Add("@ToDate", Convert.ToDateTime(ToDate));
            p.Add("@Prefix", Prefix);
            p.Add("@GroupId", GroupId);
            return DynamicRepository.All<EnrollmentList>("Sp_EnrollmentList", p);
        }
        public Enrollments EditEnrollment(int EnrollmentId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@EnrollmentId", EnrollmentId);
            return DynamicRepository.FindBy<Enrollments>("Sp_EnrollmentEdit", p);
        }
        public void ActivateOrDeactivatePerson(int Type, int PersonId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@Type", Type);
            p.Add("@PersonId", PersonId);
            DynamicRepository.FindParameters<object>("Sp_ActiveOrDeactivePerson", p);
        }
        public SelfChitList GetPersonByGroupId(int GroupId, int ChitNo)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@GroupId", GroupId);
            p.Add("@ChitNo", ChitNo);
            return DynamicRepository.FindBy<SelfChitList>("Sp_GetPersonByGroupIdChitNo", p);
        }
        public SelfChits GetSelfChitsPerson(int PersonId)
        {
            SelfChits selfChit = new SelfChits();
            DynamicParameters p = new DynamicParameters();
            DynamicParameters p1 = new DynamicParameters();
            p.Add("@PersonId", PersonId);
            p1.Add("@PersonId", PersonId);
            p1.Add("@brnid", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p1.Add("@brnwiselistno", dbType: DbType.Int32, direction: ParameterDirection.Output);
            selfChit.selfChitAddress = DynamicRepository.FindBy<SelfChitsAddress>("Persons_NameAddressRetrieve", p);
            selfChit.selfChitList = DynamicRepository.All<SelfChitList>("Sp_SelfChitRetrieve", p1);
            return selfChit;
        }
        public List<SelfChitList> GetNewSelfChitDetails(int PersonId, int GroupId, int ChitNo)
        {
            List<SelfChitList> selfChit = new List<SelfChitList>();
            DynamicParameters p = new DynamicParameters();
            p.Add("@PersonId", PersonId);
            p.Add("@GroupId", GroupId);
            p.Add("@ChitNo", ChitNo);
            selfChit= DynamicRepository.All<SelfChitList>("Sp_NewSelfChitDetails", p);
            return selfChit;
            
        }
        public object CheckExist(int SPersonId, int PersonId,int EnrollId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@SPersonId", SPersonId);
            p.Add("@PersonId", PersonId);
            p.Add("@EnrollId", EnrollId);
            p.Add("@out", dbType: DbType.Int32, direction: ParameterDirection.Output);
            DynamicRepository.FindBy<object>("SelfChit_CheckExist", p);
            return p.Get<int>("@out");
        }
        public object GetListNo(int BranchId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@brnid", BranchId);
            p.Add("@ListNo", dbType: DbType.Int32, direction: ParameterDirection.Output);
            DynamicRepository.FindBy<object>("GetListNo", p);
            return p.Get<object>("@ListNo");
        }
       public void SelfChitsEntry(List<SelfChitInsert> selfChitinsert)
        {
            
            DataTable selfChitsdt = ConvertToDataTable<SelfChitInsert>(selfChitinsert);
            DynamicParameters p = new DynamicParameters();
            p.Add("@SelfChits", selfChitsdt.AsTableValuedParameter("TVP_SelfChit"));
            p.Add("@SPersonId", dbType: DbType.Int32, direction: ParameterDirection.Output);
            DynamicRepository.FindParameters<SelfChitInsert>("SelfChitsInsertOrUpdate", p);
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
    }
}

