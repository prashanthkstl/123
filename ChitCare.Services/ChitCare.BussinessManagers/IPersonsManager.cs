﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using ChitCare.BussinessEntities.Objects;
namespace ChitCare.BussinessManagers
{
   public interface IPersonsManager
    {
        IUnitOfWork UnitOfWork { get; set; }
        IDynamicRepository DynamicRepository { get; set; }
        List<PersonsAutoList> AutoListPerson(string BrnId, string CompId, string id, string Prefix, string GrpId, string Uid, string Mode);
        void CreatePerson(Persons persons);
    }
}
