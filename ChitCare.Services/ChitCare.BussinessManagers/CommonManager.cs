﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using Dapper;
namespace ChitCare.BussinessManagers
{
   public class CommonManager:BaseManager,ICommonManager
    {
        public CommonManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }


        public List<Menu> getModuleDetails(int roleId)
        {
            return DynamicRepository.All<Menu>("sp_Menu1", roleId);
        }
        public List<Branch> GetBranches()
        {
            return DynamicRepository.All<Branch>("BranchList", null);
        }

        public List<Groups> AutoListGroups(string prefix,int BranchId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@Key", prefix);
            p.Add("@BranchId", BranchId);
            return DynamicRepository.All<Groups>("Groups_AutoFill", p);
        }
        
    }
}
