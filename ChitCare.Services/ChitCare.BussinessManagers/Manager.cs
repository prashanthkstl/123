﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
namespace ChitCare.BussinessManagers
{
   public class Manager : IManager
    {
        public IFinyearManager FinyearManager { get; set; }
        public IUserLoginManager UserLoginManager { get; set; }
        public IPersonsManager PersonsManager { get; set; }
      
        public IPlacesManager PlacesManager { get; set; }
        public IModuleScreenManager ModuleScreenManager { get; set; }
        public IGroupsManager GroupManager { get; set; }
        public Manager(IUnitOfWork _unitOfWork, IUserLoginManager _userLoginManager, IFinyearManager _finYearManager, IPersonsManager _personsManager, IPlacesManager _placesManager, IModuleScreenManager _moduleScreenManager, IGroupsManager _groupManager)
        {
            AssignFinyearManager(_unitOfWork, _finYearManager);
            AssignUserLoginManger(_unitOfWork, _userLoginManager);
            AssignPersonsManager(_unitOfWork, _personsManager);
       
            AssignPlacesManager(_unitOfWork, _placesManager);
            AssignModuleScreenManager(_unitOfWork, _moduleScreenManager);
            AssignGroupsManager(_unitOfWork, _groupManager);
        }
        public void AssignFinyearManager(IUnitOfWork _unitOfWork, IFinyearManager _finYearManager)
        {
            FinyearManager = _finYearManager;
            FinyearManager.UnitOfWork = _unitOfWork;
            FinyearManager.DynamicRepository.UnitOfWork = _unitOfWork;
        }
        public void AssignUserLoginManger(IUnitOfWork _unitOfWork, IUserLoginManager _userLoginManager)
        {
            UserLoginManager = _userLoginManager;
            UserLoginManager.UnitOfWork = _unitOfWork;
            UserLoginManager.DynamicRepository.UnitOfWork = _unitOfWork;
        }
        public void AssignPersonsManager(IUnitOfWork _unitOfWork,IPersonsManager _personsManager)
        {

            PersonsManager = _personsManager;
            PersonsManager.UnitOfWork = _unitOfWork;
            PersonsManager.DynamicRepository.UnitOfWork = _unitOfWork;
        }
       
        public void AssignPlacesManager(IUnitOfWork _unitOfWork,IPlacesManager _placesManager)
        {
            PlacesManager = _placesManager;
            PlacesManager.UnitOfWork = _unitOfWork;
            PlacesManager.DynamicRepository.UnitOfWork = _unitOfWork;
        }
        public void AssignModuleScreenManager(IUnitOfWork _unitOfWork,IModuleScreenManager _moduleScreenManager)
        {
            ModuleScreenManager = _moduleScreenManager;
            ModuleScreenManager.UnitOfWork = _unitOfWork;
            ModuleScreenManager.DynamicRepository.UnitOfWork = _unitOfWork;
        }
        public void AssignGroupsManager(IUnitOfWork _unitOfWork,IGroupsManager _groupManager)
        {
            GroupManager = _groupManager;
            GroupManager.UnitOfWork = _unitOfWork;
            GroupManager.DynamicRepository.UnitOfWork = _unitOfWork;
        }
    }
}
