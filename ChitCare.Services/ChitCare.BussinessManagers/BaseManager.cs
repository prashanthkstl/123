﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;

namespace ChitCare.BussinessManagers
{
   public class BaseManager
    {
        public IUnitOfWork UnitOfWork { get; set; }

        public IDynamicRepository DynamicRepository { get; set; }

        public void BeginTransaction()
        {
            UnitOfWork.Begin();
        }
        public void CommitTransaction()
        {
            UnitOfWork.Commit();
        }
        public void RollBackTransaction()
        {
            UnitOfWork.Rollback();
        }
    }
}
