﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChitCare.BussinessEntities.Objects;
using ChitCare.Services.DAL;
using ChitCare.Services.DAL.Reposotories;
using Dapper;
using System.Data;
namespace ChitCare.BussinessManagers
{
   public class PlacesManager: BaseManager,IPlacesManager
    {
        public PlacesManager(IDynamicRepository _dynamicRepository)
        {
            DynamicRepository = _dynamicRepository;
        }
        #region Country Region
        public void CountryInsert(Countries countries)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@CountryId", countries.CountryId);
            p.Add("@CountryName", countries.CountryName);
            DynamicRepository.FindParameters<object>("Countries_InsertOrUpdate", p);

        }

    
        public void CountryUpdate(Countries countries)
        {
            DynamicParameters p = new DynamicParameters();
            
            p.Add("@CountryName", countries.CountryName);
            
          
            DynamicRepository.FindParameters<object>("Countries_Update", p);

        }
        public void DeleteCountry(Countries countries)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@CountryId", countries.CountryId);
            DynamicRepository.FindParameters<Countries>("Countries_Delete", p);
        }
        public List<Countries> GetCountries()
        {
            return DynamicRepository.All<Countries>("sp_CountryRetrieve", null);
        }

        public Countries CountryEdit(int CountryId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@CountryId", CountryId);
            Countries country = DynamicRepository.FindBy<Countries>("Country_Retrieve", p);
            return country;
        }
#endregion
        #region State Region
        public void StateInsert(States states)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@StateId", states.StateId);
            p.Add("@StateCountryId", states.StateCountryId);
            p.Add("@StateName", states.StateName);
         
            DynamicRepository.FindParameters<object>("States_InsertOrUpdate", p);
        }

        public List<States> StateView(int CountryId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@StateCountryId", CountryId);
            return DynamicRepository.All<States>("StatesList", p);
        }
        public List<States> GetStates()
        {
            return DynamicRepository.All<States>("sp_StateRetrieve", null);
        }

        public States StateEdit(int StateId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@StateId", StateId);
            States state = DynamicRepository.FindBy<States>("StateEditRetrieve", p);
            return state;
        }

        public void DeleteState(States states)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@StateId", states.StateId);
            DynamicRepository.FindParameters<States>("State_Delete", p);
        }
#endregion
        #region City Region
        public void CitiesInsert(Cities cities)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@CityId", cities.CityId);
            p.Add("@CityName", cities.CityName);
            p.Add("@PinCode", cities.PinCode);
            p.Add("@CityDistId", cities.CityDistrictId);
            DynamicRepository.FindParameters<object>("Cities_InsertOrUpdate", p);
        }

        public List<Cities> CityView(int CityDistrictId, string CityName)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@CityDistId", CityDistrictId);
            p.Add("@CityName", CityName);
            return DynamicRepository.All<Cities>("CityList", p);
        }

        public Cities CityEdit(int CityId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@CityId", CityId);
            Cities city = DynamicRepository.FindBy<Cities>("City_Retrieve", p);
            return city;
        }
        public  void CitiesUpdate(Cities cities)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@out", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("@CityId", cities.CityId);
            
            p.Add("@CityName", cities.CityName);
          
            p.Add("@PinCode", cities.PinCode);
            DynamicRepository.FindParameters<object>("Cities_Update", p);
        }
        public void DeleteCity(Cities cities)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@CityId", cities.CityId);
            DynamicRepository.FindParameters<Countries>("Cities_Delete", p);
        }
        public List<Cities> GetCities()
        {
            return DynamicRepository.All<Cities>("sp_CitiesRetrive", null);
        }
#endregion
        #region District Region
        public void DistInsert(Districts dist)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@DistrictId", dist.DistrictId);
            p.Add("@DistrictStateId", dist.DistrictStateId);
            p.Add("@DistrictName", dist.DistrictName);
            DynamicRepository.FindParameters<object>("Districts_InsertOrUpdate", p);
        }

        public List<Districts> DistrictView(int StateId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@DistrictStateId", StateId);
            return DynamicRepository.All<Districts>("DistrictList", p);
        }
        public void DistUpdate(Districts dist)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@out", dbType: DbType.Int32, direction: ParameterDirection.Output);
            p.Add("@DistrictId", dist.DistrictId);
           
            p.Add("@DistrictName", dist.DistrictName);
         
           
            DynamicRepository.FindParameters<object>("Districts_Update", p);
        }
        public void DeleteDist(Districts dist)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@DistrictId", dist.DistrictId);
            DynamicRepository.FindParameters<Countries>("Districts_Delete", p);
        }
        public List<Districts> GetDistricts()
        {
            return DynamicRepository.All<Districts>("sp_DistrictRetrieve", null);
        }

        public Districts DistrictEdit(int DistrictId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@DistrictId", DistrictId);
            Districts districts = DynamicRepository.FindBy<Districts>("DistrictEditRetrieve", p);
            return districts;
        }
        #endregion
        #region Route Region
        public void RouteInsert(Routes route)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@RouteID", route.RouteID);
            p.Add("@RouteBranchId", route.RouteBranchId);
            p.Add("@RouteName", route.RouteName);
            DynamicRepository.FindParameters<object>("Routes_InsertOrUpdate", p);
        }


        public List<Routes> RouteView(int BranchId)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@RouteBranchId", BranchId);
            return DynamicRepository.All<Routes>("RoutesList", p);
        }

        public Routes RouteEdit(int RouteID)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@RouteID", RouteID);
            Routes route = DynamicRepository.FindBy<Routes>("RouteEditRetrieve", p);
            return route;
        }
        #endregion
        #region Area Region
        public void AreaEntry(Areas area)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@AreaId", area.AreaId);
            p.Add("@AreaBranchId", area.AreaBranchId);
            p.Add("@AreaRouteId", area.AreaRouteId);
            p.Add("@AreaName", area.AreaName);
            DynamicRepository.FindParameters<object>("Areas_InsertOrUpdate", p);
        }

        public List<Areas> AreaView(int RouteID)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@RouteID", RouteID);
            return DynamicRepository.All<Areas>("AreasList", p);
        }

        public Areas AreaEdit(int AreaID)
        {
            DynamicParameters p = new DynamicParameters();
            p.Add("@AreaID", AreaID);
            Areas area = DynamicRepository.FindBy<Areas>("AreaEditRetrieve", p);
            return area;
        }

        public List<Routes> GetRoutes()
        {
            return DynamicRepository.All<Routes>("sp_RouteRetrieve", null);
        }

        #endregion

    }
}
