﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.UI.ViewModels.Models
{
  public class BranchViewModel
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }

    }
}
