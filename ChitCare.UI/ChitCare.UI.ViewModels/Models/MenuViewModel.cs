﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.UI.ViewModels.Models
{
    public class MenuViewModel
    {

        public int moduleId { get; set; }
        public int subModuleId { get; set; }
        public string moduleName { get; set; }
        public string subModuleName { get; set; }
        public string addUrl { get; set; }
        public string screenName { get; set; }
        public string ReportImage { get; set; }
        public string ViewUrl { get; set; }
        public string image { get; set; }
        public int screenId { get; set; }

    }




}
