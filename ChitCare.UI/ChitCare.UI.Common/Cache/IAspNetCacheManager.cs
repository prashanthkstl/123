﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChitCare.UI.Common.Cache
{
   public interface IAspNetCacheManager
    {
        T GetCache<T>(string key);
        void AddCache(string key, object value);
        T GetApplicationCache<T>(string key);
        void AddApplicationCache(string key, object value);
    }
}
