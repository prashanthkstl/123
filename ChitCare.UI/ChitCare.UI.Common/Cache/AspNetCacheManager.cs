﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ChitCare.UI.Common.Cache
{
   public class AspNetCacheManager : IAspNetCacheManager
    {
        public T GetCache<T>(string key)
        {
            object tempCacheValue = HttpContext.Current.Cache.Get(key);

            if (tempCacheValue is T)
                return (T)tempCacheValue;

            return default(T);
        }

        public void AddCache(string key, object value)
        {
            HttpContext.Current.Cache.Insert(key, value, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.Zero);
        }

        public T GetApplicationCache<T>(string key)
        {
            object tempCacheValue = HttpContext.Current.Application[key];

            if (tempCacheValue is T)
                return (T)tempCacheValue;

            return default(T);
        }

        public void AddApplicationCache(string key, object value)
        {
            HttpContext.Current.Application[key] = value;
        }
    }
}

