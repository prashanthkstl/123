﻿using System.Threading.Tasks;
using System.Net.Http;


namespace ChitCare.UI.Common.SimpleHttpClient
{
    public interface ICHITCAREHttpClient
    {
        HttpClient HttpClient { get; }
        T GetRequestSync<T>(string actionUrl, int? Id = null, string multiFormURIQueryString = "");
        U SendRequestSync<T, U>(string actionUrl, T viewModel);
        Task<T> GetRequest<T>(string actionUrl, int? Id = null, string multiFormURIQueryString = "");
        Task<U> SendRequest<T, U>(string actionUrl, T viewModel);
        Task<object> GetRequest(string actionUrl, int? Id = null, string multiFormURIQueryString = "")
    }
}
