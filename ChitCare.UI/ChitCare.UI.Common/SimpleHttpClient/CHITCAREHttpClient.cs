﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace ChitCare.UI.Common.SimpleHttpClient
{
   public class CHITCAREHttpClient : ICHITCAREHttpClient
    {
        private HttpClient _httpClient;

        public HttpClient HttpClient
        {
            get
            {
                return _httpClient;
            }
        }

        public CHITCAREHttpClient()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(ConfigurationManager.AppSettings["ServicesURL"].ToString());
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<T> GetRequest<T>(string actionUrl, int? Id = null, string multiFormURIQueryString = "")
        {
            T result = default(T);

            if (Id.HasValue)
                actionUrl = string.Format(actionUrl + "?Id={0}", Id);
            else if (!string.IsNullOrWhiteSpace(multiFormURIQueryString))
                actionUrl = actionUrl + "?" + multiFormURIQueryString;

            HttpResponseMessage response = await HttpClient.GetAsync(actionUrl);

            if (response.IsSuccessStatusCode)
                result = response.Content.ReadAsAsync<T>().Result;
            else
                throw new Exception(response.Content.ReadAsStringAsync().Result);

            return result;
        }

        public T GetRequestSync<T>(string actionUrl, int? Id = null, string multiFormURIQueryString = "")
        {
            T result = default(T);
            
            if (Id.HasValue)
                actionUrl = string.Format(actionUrl + "?id={0}", Id);
            else if (!string.IsNullOrWhiteSpace(multiFormURIQueryString))
                actionUrl = actionUrl + "?" + multiFormURIQueryString;

            HttpResponseMessage response = HttpClient.GetAsync(actionUrl).Result;

            if (response.IsSuccessStatusCode)
                result = response.Content.ReadAsAsync<T>().Result;
            else
                throw new Exception(response.Content.ReadAsStringAsync().Result);

            return result;
        }
        public async Task<object> GetRequest(string actionUrl, int? Id = null, string multiFormURIQueryString = "")
        {
            object result = default(object);

            if (Id.HasValue)
                actionUrl = string.Format(actionUrl + "?Id={0}", Id);
            else if (!string.IsNullOrWhiteSpace(multiFormURIQueryString))
                actionUrl = actionUrl + "?" + multiFormURIQueryString;

            HttpResponseMessage response = await HttpClient.GetAsync(actionUrl);

            if (response.IsSuccessStatusCode)
                result = response.Content.ReadAsAsync<object>().Result;
            else
                throw new Exception(response.Content.ReadAsStringAsync().Result);

            return result;
        }
        public async Task<U> SendRequest<T, U>(string actionUrl, T viewModel)
        {
            U result = default(U);

            HttpResponseMessage response = await HttpClient.PostAsJsonAsync(actionUrl, viewModel);

            if (response.IsSuccessStatusCode)
                result = response.Content.ReadAsAsync<U>().Result;
            else
                throw new Exception(response.Content.ReadAsStringAsync().Result);

            return result;
        }

        public U SendRequestSync<T, U>(string actionUrl, T viewModel)
        {
            U result = default(U);

            HttpResponseMessage response = HttpClient.PostAsJsonAsync(actionUrl, viewModel).Result;

            if (response.IsSuccessStatusCode)
                result = response.Content.ReadAsAsync<U>().Result;
            else
                throw new Exception(response.Content.ReadAsStringAsync().Result);

            return result;
        }

    }
}

