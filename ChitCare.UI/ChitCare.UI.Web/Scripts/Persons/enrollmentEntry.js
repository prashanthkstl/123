﻿var app = angular.module('myEnrollMent', []);

app.controller('enrollmentController', function ($scope, $window, $http, $filter) {
    $scope.ddlPayMode = {};
    $scope.ddlICardType = {};
    $scope.ddlIntType = {};
    $scope.ddlIntType.IntemationTypes = [{ text: "Collection Agent", value: 1 }, { text: "Bussiness Agent", value: 2 }, { text: "Self Chits(GroupList)", value: 3 }];
    $scope.ddlICardType.IntemationCardTypes = [{ text: "Post", value: 1 }, { text: "Hand", value: 2 }, { text: "Courior", value: 3 }, { text: "EMail", value: 4 }, { text: "NO", value: 5 }];
    $scope.ddlICardType.value = $scope.ddlICardType.IntemationCardTypes[0].value;
    $scope.ddlPayMode.PayModes = [{ text: "Monthly", value: 1 }, { text: "Weekly", value: 2 }, { text: "Daily", value: 3 }];
    $scope.ddlPayMode.value = $scope.ddlPayMode.PayModes[0].value;
    $scope.ddlIntType.value = $scope.ddlIntType.IntemationTypes[0].value;
    var Pid = 0;
    var BussinessAgentId = 0;
    var CollectionAgentId = 0;
    var GroupId = 0;
    var Enrollments = {};


   
    $window.onload = function ()
    {

       

        $http.get('/Common/GetBranches').success(function (branchesResponse) {
            $scope.enrlBranch = {};
            $scope.ddlBranch = {};
            $scope.enrlBranch.Branches = branchesResponse;
            $scope.ddlBranch.Branches = branchesResponse;
            $scope.enrlBranch.BranchId = $scope.enrlBranch.Branches[0].BranchId;
            $scope.ddlBranch.BranchId = $scope.ddlBranch.Branches[0].BranchId;
            
        })

        if (EnrollId > 0)
        {

            alert(EnrollId)
            $http.get('/Masters/EnrollmentEdit?EnrollmentId=' + EnrollId).then(function (data) {
                Enrollments = data.data;
              
                console.log(Enrollments);
                alert(data.data.ChitNo)
                GetVacantChits(data.data.GroupId, (data.data.Instalments-data.data.ChitNo));
                $scope.enrlDate =new Date(data.data.EnrollmentDate.match(/\d+/)[0] * 1);
                $scope.enrlBranch.BranchId = data.data.BranchId;
                            //GroupId: GroupId,
               // $scope.ddlEnlTkt.ChitNo = $scope.ddlEnlTkt.Tickets[data.data.ChitNo].ChitNo;
               // Enrollments.PersonId = data.data.PersonId;
                $scope.ddlPayMode.value = data.data.PaymentType;
                $scope.ddlICardType.value = data.data.IC_Type;                     
                $scope.txtNName = data.data.Nomineename;
                $scope.txtNRelation = data.data.NomineeRelation;
                $scope.txtNAge = data.data.NomineeAge;
                                                                    //AgentId: BussinessAgentId,
                $scope.chkEnrlStatus = data.data.Eligibility;
                $scope.enrlGropup = data.data.GroupName;                                                         
                $scope.txtSubName = data.data.SubscriberName;                                                               //StaffID: CollectionAgentId,
                $scope.txtBussAgent = data.data.BussinessAgent;
                $scope.txtCollAgent = data.data.CollectionAgent;
                $scope.chkRon = data.data.AgrmtReceived;
                $scope.EnrlRoDate = new Date(data.data.AgrmtDate.match(/\d+/)[0] * 1);
                $scope.chkIBP = data.data.IsPBIssued;
                $scope.ibpDate = new Date(data.data.PBIssusedDate.match(/\d+/)[0] * 1);                                                                              
                $scope.txtNDoorNo = data.data.NomineeAddress1;
                $scope.txtNStreetName = data.data.NomineeAddress2;
                $scope.txtNAddress = data.data.NomineeAddress3;
                $scope.txtNCity = data.data.CityName;
                $scope.District = data.data.DistrictName;
                $scope.State = data.data.StateName;
                $scope.Country = data.data.countryName;
                $scope.Pincode = data.data.NomineePincode;
                $scope.txtNPhone = data.data.NomineePhoneNo;
                                                                                                                                          
                $scope.rbAddressCorr = data.data.AddressCorr;
                                                                                                                                                                        
                $scope.txtIncentive = data.data.IntencivePercentage;
                $scope.chkIncentive = data.data.IntenciveAllow;
                $scope.txtIntrIncent = data.data.IntroducerInc;
                                                                                                                                                                                                                                    
                $scope.ddlIntType.value = data.data.Intimate_Type;
                                                                                                                                                                                                                                                   
            });

        }
    }
    $scope.EnrollmentList = function ()
    {
        window.location = '/Masters/EnrollmentList';
    }
    $scope.GetId = function () {

        alert($scope.enrlBranch.BranchId);
    }
    $scope.completeGroup = function (string)
    {
        $scope.hidethis = false;
        var output = [];
        $http.get('/Common/GroupsAutoList?prefix=' + $scope.enrlGropup + '&BranchId=' + $scope.ddlBranch.BranchId + '').then(function (groupsResponse) {

           
            if (groupsResponse.data.length > 0) {
                $scope.groupList = groupsResponse.data;
                
            }
            else {
               
                $scope.hidethis = true;
            }
            console.log(groupsResponse.data);
        });
        angular.forEach($scope.groupList, function (group) {

            if (group.GroupName.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                
                output.push(group);
            }
        });
        $scope.filterGroup = output;
    }

    $scope.fillTextboxGroup = function (groupName, id, ChitNumber) {
        ChitNo = ChitNumber;
        $scope.enrlGropup = groupName;
        $scope.hidethis = true;
        Enrollments.GroupId = id;
        if (id > 0) {
            GetVacantChits(id,0);
        }
        else {

        }
    }
    GetVacantChits = function (GId,selected)
    {
        $http.get('/Masters/GetVacantChits?GroupId=' + GId).then(function (response) {

            $scope.ddlEnlTkt = {};
            $scope.ddlEnlTkt.Tickets = response.data;
            $scope.ddlEnlTkt.ChitNo = $scope.ddlEnlTkt.Tickets[selected].ChitNo;
        });
    }
    $scope.PersonAutoList = function (string, Type) {

        var output = [];
        var BranchId = $scope.enrlBranch.BranchId;
        
        var CompanyId = 0;
        var Id = 0;
        var GroupId = 0;
        var UserId = 0;
        if (Type == 'SubName')
        {
            $scope.hidethisP = false;
            $scope.hidethisBussAgent = true;
            $scope.hidethisCollAgent = true;
            var Mode = 1;
            var prefix = $scope.txtSubName;
        }
        if (Type == 'BussAgent') {
            $scope.hidethisP = true;
            $scope.hidethisBussAgent = false;
            $scope.hidethisCollAgent = true;
            var Mode = 2;
            var prefix = $scope.txtBussAgent;
        }
        if (Type == 'CollAgent') {
            $scope.hidethisP = true;
            $scope.hidethisBussAgent = true;
            $scope.hidethisCollAgent = false;
            var Mode = 3;
            var prefix = $scope.txtCollAgent;
        }
        
    

        $http.get('/AutoComplete/PersonAutoList?BranchId=' + BranchId + ' &CompanyId=' + CompanyId + ' &Id=' + Id + ' &Prefix=' + prefix + '&GroupId=' + GroupId + '&UserId=' + UserId + ' &Mode=' + Mode).then(function (personsResponse) {


            if (personsResponse.data.length > 0) {
                $scope.PersonList = personsResponse.data;

            }
            else {
                if (Type == 'SubName') {
                    $scope.hidethisP = true;
                   
                }
                if (Type == 'BussAgent') {
                   
                    $scope.hidethisBussAgent = true;
                   
                }
                if (Type == 'CollAgent') {
                  
                    $scope.hidethisCollAgent = true;
                    
                }
                
            }
            console.log(personsResponse.data);
        });
        angular.forEach($scope.PersonList, function (person) {

            if (person.Name.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                //output.push(country.preFix);
                output.push(person);
            }
        });
        $scope.filterPerson = output;
    }

    $scope.fillPersonTextbox = function (Name, id,Type) {

        if (Type == 'Subscriber') {
            $scope.txtSubName = Name;
            $scope.hidethisP = true;

        }
        if (Type == 'Bussiness') {
            $scope.txtBussAgent = Name;
            $scope.hidethisBussAgent = true;
            
        }
        if (Type == 'Collection') {
            $scope.txtCollAgent = Name;
            $scope.hidethisCollAgent = true;
           
        }
        
       

        if (id > 0) {
            if (Type == 'Subscriber') {
 
                Enrollments.PersonId = id;

            }
            if (Type == 'Bussiness') {
               
                Enrollments.AgentId = id;

            }
            if (Type == 'Collection') {
                Enrollments.StaffID = id;

            }
          
          
        }
        else {

        }
    }

    $scope.FillNomineeAddress = function ()
    {
       
        if ($scope.chkFillSubAddr == true)
        {
            
            if (Enrollments.PersonId > 0)
            {
                
                $http.get('/AutoComplete/GetPlacesByPersonId?PersonId=' + Enrollments.PersonId).then(function (data) {


                    console.log(data.data);
                    $scope.txtNCity = data.data.CityName;
                    $scope.Country = data.data.countryName;
                    $scope.District = data.data.DistrictName;
                    $scope.Pincode = data.data.PinCode;
                    $scope.State = data.data.StateName;
                    $scope.txtNDoorNo = data.data.Address;
                    $scope.txtNAddress = data.data.Address2;
                    $scope.txtNStreetName = data.data.Address1;
                    Enrollments.NomineeCity = data.data.CityId;
                    Enrollments.NomineeDistrict = data.data.DistrictId;
                    Enrollments.NomineeState = data.data.StateId;
                    Enrollments.NomineeCountry = data.data.CountryId;
                });
            }
           
          
        }
        else {
            $scope.txtNCity = "";
            $scope.Country = "";
            $scope.District = "";
            $scope.Pincode = "";
            $scope.State = "";
            $scope.txtNDoorNo = "";
            $scope.txtNAddress = "";
            $scope.txtNStreetName = "";

        }
    }
    $scope.SbEnrollMentEntry = function ()
    {
       
        var Enrollment = {

            EnrollmentId: EnrollId,
            EnrollmentDate: $scope.enrlDate,
            BranchId: $scope.enrlBranch.BranchId,
            GroupId: Enrollments.GroupId,
            ChitNo: $scope.ddlEnlTkt.ChitNo,
            PersonId: Enrollments.PersonId,
            PaymentType: $scope.ddlPayMode.value,
            SF_Setup: 1,
            Own_Chit: 1,
            Rep_By: "ABC",
            Nomineename: $scope.txtNName,
            NomineeRelation: $scope.txtNRelation,
            NomineeAge: $scope.txtNAge,
            AgentId: Enrollments.AgentId,
            Eligibility: $scope.chkEnrlStatus,
            IC_Print: 1,
            IC_Type: $scope.ddlICardType.value,
            StaffID: Enrollments.StaffID,
            EnrolPosition: 1,
            CancelId: 1,
            BadDebitor: 1,
            AgrmtReceived: $scope.chkRon,
            AgrmtDate: new Date($scope.EnrlRoDate),
            IsPBIssued: $scope.chkIBP,
            PBIssusedDate: new Date($scope.ibpDate),
            NomineeAddress1: $scope.txtNDoorNo,
            NomineeAddress2: $scope.txtNStreetName,
            NomineeAddress3: $scope.txtNAddress,
            NomineeCity: Enrollments.NomineeCity,
            NomineeDistrict: Enrollments.NomineeDistrict,
            NomineeState: Enrollments.NomineeState,
            NomineeCountry: Enrollments.NomineeCountry,
            NomineePincode: $scope.Pincode,
            NomineePhoneNo: $scope.txtNPhone,
            Payable: 131124,
            Paid: 241241,
            Balance: 1241414,
            AddressCorr: $scope.rbAddressCorr,
            SFDate: new Date(),
            PSDate: new Date(),
            CreatedBy: 70,
            CreatedDate: new Date(),
            ModifiedBy: 70,
            ModifiedDate: new Date(),
            AreaId: 1,
            OutVisible: 1,
            IntencivePercentage: $scope.txtIncentive,
            IntenciveAllow: $scope.chkIncentive,
            SF:"abcd",
            SuitNo: 1,
            IsSecondAccount: 1,
            SecondACDate: new Date(),
            IntroducerInc: $scope.txtIntrIncent,
            UnderRecovery: 1,
            zoneid: 1,
            DirectorId: 1,
            Intimate_Type: $scope.ddlIntType.value,
            cusomerid: 1237,
            Passbookno: "Prashanth",
        }
       

        var post = $http({
            method: "POST",
            url: "/Masters/EnrollmentEntry",
            dataType: 'json',
            data: { enrollment: Enrollment },
            headers: { "Content-Type": "application/json" }
        });

        post.success(function () {

           
                alert("Enrollment Registration Succefully Done..")
          
                window.location = '/Masters/EnrollmentEntry'
        });

        //post.error(function (data, status) {
        //    $window.alert(data.Message);
        //});
    }

    $scope.complete = function (string, Type) {

            $scope.hidethis = false;
           


        var output = [];



        $http({
            url: '/AutoComplete/CityAutoList',
            method: "POST",
            data: {

                prefix: $scope.txtNCity

            },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function (response) {


                if (response.data.length > 0) {
                    $scope.cityList = response.data;
                }
                else {
                   
                        $scope.hidethis = true;
                       
                }
                // $scope.cityList = response.data;
            },
            function (response) { // optional
                $scope.hidethis = true;
                $scope.autolistId = 0;
                console.log("Failed");
            });
        
        angular.forEach($scope.cityList, function (city) {

            if (city.CityName.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                //output.push(country.preFix);
                output.push(city);
            }
        });
        $scope.filterCity = output;

    }

    $scope.fillTextbox = function (string, id) {
        
        $scope.txtNCity = string;
           
            $scope.hidethis = true;
     
        if (id > 0) {
            GetPlaces(id);
        }
        else {
            
        }
    }
    GetPlaces = function (id) {
        $http.get('/AutoComplete/GetPlacesByCityId?CityId=' + id).then(function (data) {

            Enrollments.NomineeCity = data.data.CityId;
            Enrollments.NomineeState = data.data.StateId;
            Enrollments.NomineeDistrict = data.data.DistrictId;
            Enrollments.NomineeContry = data.data.CountryId;
            $scope.txtNCity = data.data.CityName;
            $scope.Country = data.data.countryName;
            $scope.District = data.data.DistrictName;
            $scope.Pincode = data.data.PinCode;
            $scope.State = data.data.StateName;

          

        });
    }
});