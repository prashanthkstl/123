﻿
var app = angular.module("EnrollmentListApp", []);

app.controller("MyChitCareEnrollmentList", function ($scope, $http, $window, $filter) {

    $scope.showListByPerson = true;
    $scope.PageSize = [{ text: "2", value: 2 }, { text: "10", value: 10 }, { text: "20", value: 20 }];
    $scope.page = $scope.PageSize[0];
    $scope.IsVisible = false;
    $scope.pageSize = 2;

    $scope.start = 0;
    $scope.end = $scope.pageSize;
    $scope.DateRange = [{ value: 1, text: "All" }, { value: 2, text: "Today" }, { value: 3, text: "Current Week" }, { value: 4, text: "Current Month" }, { value: 5, text: "Current Quarter" }, { value: 6, text: "Current Year" }, { value: 7, text: "Date to Date" }, { value: 8, text: "Yester Day" }, { value: 9, text: "Previous Year" }, { value: 10, text: "Previous Month" }];
   
   
    $scope.ddlConRange = $scope.DateRange[0];
    $scope.txtFromDate = new Date();
    $scope.txtToDate = new Date();
    var GroupId = 0;
    $scope.ActivateOrDeactivate = function (string) {
        alert(string)


    }
    $window.onload = function () {
        $http.get('/Common/GetBranches').then(function (branchesResponse) {

            $scope.ddlBranch = {};

            $scope.ddlBranch.Brancesddl = branchesResponse.data;

            $scope.ddlBranch.BranchId = $scope.ddlBranch.Brancesddl[0].BranchId;

        })
    }


   
    $scope.GetDateRange = function () {
        if ($scope.ddlConRange.value == 7) {
            $scope.showDatetoDate = true;
        } else {
            $scope.showDatetoDate = false;
        }
    }

    $scope.GetEnrollments = function () {
        alert(ListType)
        $scope.showBar = true;
        var BranchId = $scope.ddlBranch.BranchId;
        var Prefix = $scope.txtPersonName;
        var FromDate = $filter('date')($scope.txtFromDate, "yyyy-MM-dd");
        var ToDate = $filter('date')($scope.txtToDate, "yyyy-MM-dd");



        $http.get("/Masters/GetEnrollmentList?BranchId=" + BranchId + "&Prefix=" + Prefix + "&Fdate=" + FromDate + "&ToDate=" + ToDate + "&GroupId=" + GroupId + "").success(function (data) {
            console.log(data.data);
            $scope.Enrollments = data;
            $scope.tblShow = true;
            $scope.Total = $scope.Enrollments.length;
            $scope.showBar = false;
            var Svalue1 = parseInt($scope.Enrollments.length / $scope.page.value);
            var Svalue2 = $scope.Enrollments.length / $scope.page.value;
            var NoOPages;
            if (Svalue2 > Svalue1) {
                NoOPages = Svalue1 + 1;
            }
            else {
                NoOPages = Svalue1;
            }
            $scope.Lpage = NoOPages;
            $scope.PageNum = 1;
        }).error(function (error) {
            console.log(error);
        });


    }

    $scope.Psize = function () {
        var Svalue1 = parseInt($scope.Enrollments.length / $scope.page.value);
        var Svalue2 = $scope.Enrollments.length / $scope.page.value;
        var NoOPages;
        if (Svalue2 > Svalue1) {
            NoOPages = Svalue1 + 1;
        }
        else {
            NoOPages = Svalue1;
        }
        $scope.Lpage = NoOPages;
        $scope.end = $scope.page.value;
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;
    }
    $scope.PFChange = function (end, page) {
        $scope.start = 0;

        $scope.end = page;

        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    $scope.PNChange = function (start, end, value) {
        var Svalue1 = parseInt($scope.Enrollments.length / value);
        var Svalue2 = $scope.Enrollments.length / value;
        var startVal;
        if (Svalue2 > Svalue1) {
            startVal = Svalue1 * value;
        }
        else {
            startVal = (Svalue1 * value) - value;
        }
        if (start < startVal) {
            $scope.start = parseInt(start) + parseInt(value);

            $scope.end = parseInt(value);
        }
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;
    }
    $scope.PPChange = function (start, end, value) {
        if (start != 0) {
            $scope.start = parseInt(start) - parseInt(value);

            $scope.end = parseInt(value);
        }
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    $scope.PLChange = function (start, end, value) {


        var Svalue1 = parseInt($scope.Enrollments.length / value);
        var Svalue2 = $scope.Enrollments.length / value;
        var startVal;
        if (Svalue2 > Svalue1) {
            startVal = Svalue1 * value;
        }
        else {
            startVal = (Svalue1 * value) - value;
        }
        $scope.start = startVal;
        $scope.end = parseInt(value);
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    $scope.GoPage = function () {

        var Svalue1 = parseInt($scope.Enrollments.length / $scope.page.value);
        var Svalue2 = $scope.Enrollments.length / $scope.page.value;
        var NoOPages;
        if (Svalue2 > Svalue1) {
            NoOPages = Svalue1 + 1;
        }
        else {
            NoOPages = Svalue1;
        }

        if ($scope.txtBasicSal != "") {
            if ($scope.txtBasicSal <= NoOPages) {
                $scope.start = ($scope.txtBasicSal * $scope.page.value) - ($scope.page.value);

                $scope.end = $scope.page.value;
            }
            else {
                alert("Please Enter Valid Page Number");
            }
        }
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    //$scope.Edit = function (personId) {

    //    window.location = "/Masters/RegistrationEntry?id=" + personId;
    //}
    $scope.Edit = function () {

        window.location = "/Masters/EnrollmentEntry?id=" + $scope.id;
    }
    $scope.DeActivatedList = function () {
        var ListId = 2;
        window.location = '/Masters/RegistrationList?Type=' + ListId;
    }

    $scope.complete = function (string) {
        $scope.hidethis = false;
        var output = [];
        $http.get('/Common/GroupsAutoList?prefix=' + $scope.enrlGropup + '&BranchId=' + $scope.ddlBranch.BranchId + '').then(function (groupsResponse) {


            if (groupsResponse.data.length > 0) {
                $scope.groupList = groupsResponse.data;

            }
            else {

                $scope.hidethis = true;
            }
            console.log(groupsResponse.data);
        });
        angular.forEach($scope.groupList, function (group) {

            if (group.GroupName.toLowerCase().indexOf(string.toLowerCase()) >= 0) {

                output.push(group);
            }
        });
        $scope.filterGroup = output;
    }

    $scope.fillTextbox = function (groupName, id) {
        
        $scope.enrlGropup = groupName;
        $scope.hidethis = true;
        GroupId = id;
        if (id > 0) {
           
        }
        else {

        }
    }
});
app.filter('jsonDate', ['$filter', function ($filter) {
    return function (input, format) {
        return (input) ? $filter('date')(parseInt(input.substr(6)), format) : '';

    };
}]);