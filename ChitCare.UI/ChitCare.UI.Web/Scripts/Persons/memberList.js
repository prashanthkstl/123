﻿
var app = angular.module("PersonListApp", []);

app.controller("MyChitCarePersonList", function ($scope, $http, $window, $filter) {

    $scope.showListByPerson = true;
    $scope.PageSize = [{ text: "2", value: 2 }, { text: "10", value: 10 }, { text: "20", value: 20 }];
    $scope.page = $scope.PageSize[0];
    $scope.IsVisible = false;
    $scope.pageSize = 2;

    $scope.start = 0;
    $scope.end = $scope.pageSize;
    $scope.DateRange = [{ value: 1, text: "All" }, { value: 2, text: "Today" }, { value: 3, text: "Current Week" }, { value: 4, text: "Current Month" }, { value: 5, text: "Current Quarter" }, { value: 6, text: "Current Year" }, { value: 7, text: "Date to Date" }, { value: 8, text: "Yester Day" }, { value: 9, text: "Previous Year" }, { value: 10, text: "Previous Month" }];
    $scope.Typesddl = [{ value: 1, text: "Person wise" }, { value: 2, text: "Group wise" }];

    $scope.ddlSearchType = $scope.Typesddl[0];
    $scope.ddlConRange = $scope.DateRange[0];
    $scope.txtFromDate = new Date();
    $scope.txtToDate = new Date();
    $scope.ActivateOrDeactivate = function (Type) {

        if ($scope.id > 0) {

            $http.get('/Masters/ActivateOrDeactivatePerson?Type=' + Type + '&PersonId=' + $scope.id + '').then(function () {



                if (Type == 1) {
                    alert("Record Successfully Activated.")
                }
                else {
                    alert("Record Successfully Deactivated.")
                }
                var BranchId = $scope.ddlBranch.BranchId;
                var Prefix = $scope.txtPersonName;
                var FromDate = $filter('date')($scope.txtFromDate, "yyyy-MM-dd");
                var ToDate = $filter('date')($scope.txtToDate, "yyyy-MM-dd");

                GetPersons(BranchId, Prefix, FromDate, ToDate);
            })

        }
        else {

            alert("Please select the Person")
        }


    }
    $window.onload = function () {
        if (ListType == 1) {

            $scope.ActOrDeAct = "DectivatedList";
            $scope.ListName = "New Members List";
        }
        else {

            $scope.ActOrDeAct = "ActivatedList";
            $scope.ListName = "Deactivated Members List";
        }
        $http.get('/Common/GetBranches').then(function (branchesResponse) {

            $scope.ddlBranch = {};

            $scope.ddlBranch.Brancesddl = branchesResponse.data;

            $scope.ddlBranch.BranchId = $scope.ddlBranch.Brancesddl[0].BranchId;

        })
    }


    $scope.SearchType = function () {

        if ($scope.ddlSearchType.value === 1) {
            $scope.showListByPerson = true;
            $scope.showListByGroup = false;
        } else {
            $scope.showListByPerson = false;
            $scope.showListByGroup = true;
        }
    }
    $scope.GetDateRange = function () {
        if ($scope.ddlConRange.value == 7) {
            $scope.showDatetoDate = true;
        } else {
            $scope.showDatetoDate = false;
        }
    }

    $scope.GePersons = function () {
        alert(ListType)
        $scope.showBar = true;
        var BranchId = $scope.ddlBranch.BranchId;
        var Prefix = $scope.txtPersonName;
        var FromDate = $filter('date')($scope.txtFromDate, "yyyy-MM-dd");
        var ToDate = $filter('date')($scope.txtToDate, "yyyy-MM-dd");

        GetPersons(BranchId, Prefix, FromDate, ToDate);




    }
    $scope.NewPerson = function () {
        window.location = "/Masters/RegistrationEntry";
    }
    GetPersons = function (BranchId, Prefix, FromDate, ToDate) {
        $http.get("/Masters/GetRegistrationList?BranchId=" + BranchId + "&Prefix=" + Prefix + "&Fdate=" + FromDate + "&ToDate=" + ToDate + "&ListType=" + ListType + "").success(function (data) {
            console.log(data.data);
            $scope.Persons = data;
            $scope.tblShow = true;
            $scope.Total = $scope.Persons.length;
            $scope.showBar = false;
            var Svalue1 = parseInt($scope.Persons.length / $scope.page.value);
            var Svalue2 = $scope.Persons.length / $scope.page.value;
            var NoOPages;
            if (Svalue2 > Svalue1) {
                NoOPages = Svalue1 + 1;
            }
            else {
                NoOPages = Svalue1;
            }
            $scope.Lpage = NoOPages;
            $scope.PageNum = 1;
        }).error(function (error) {
            console.log(error);
        });

    }
    $scope.Psize = function () {
        var Svalue1 = parseInt($scope.Persons.length / $scope.page.value);
        var Svalue2 = $scope.Persons.length / $scope.page.value;
        var NoOPages;
        if (Svalue2 > Svalue1) {
            NoOPages = Svalue1 + 1;
        }
        else {
            NoOPages = Svalue1;
        }
        $scope.Lpage = NoOPages;
        $scope.end = $scope.page.value;
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;
    }
    $scope.PFChange = function (end, page) {
        $scope.start = 0;

        $scope.end = page;

        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    $scope.PNChange = function (start, end, value) {
        var Svalue1 = parseInt($scope.Persons.length / value);
        var Svalue2 = $scope.Persons.length / value;
        var startVal;
        if (Svalue2 > Svalue1) {
            startVal = Svalue1 * value;
        }
        else {
            startVal = (Svalue1 * value) - value;
        }
        if (start < startVal) {
            $scope.start = parseInt(start) + parseInt(value);

            $scope.end = parseInt(value);
        }
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;
    }
    $scope.PPChange = function (start, end, value) {
        if (start != 0) {
            $scope.start = parseInt(start) - parseInt(value);

            $scope.end = parseInt(value);
        }
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    $scope.PLChange = function (start, end, value) {


        var Svalue1 = parseInt($scope.Persons.length / value);
        var Svalue2 = $scope.Persons.length / value;
        var startVal;
        if (Svalue2 > Svalue1) {
            startVal = Svalue1 * value;
        }
        else {
            startVal = (Svalue1 * value) - value;
        }
        $scope.start = startVal;
        $scope.end = parseInt(value);
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    $scope.GoPage = function () {

        var Svalue1 = parseInt($scope.Persons.length / $scope.page.value);
        var Svalue2 = $scope.Persons.length / $scope.page.value;
        var NoOPages;
        if (Svalue2 > Svalue1) {
            NoOPages = Svalue1 + 1;
        }
        else {
            NoOPages = Svalue1;
        }

        if ($scope.txtBasicSal != "") {
            if ($scope.txtBasicSal <= NoOPages) {
                $scope.start = ($scope.txtBasicSal * $scope.page.value) - ($scope.page.value);

                $scope.end = $scope.page.value;
            }
            else {
                alert("Please Enter Valid Page Number");
            }
        }
        $scope.PageNum = parseInt($scope.start / $scope.page.value) + 1;

    }
    //$scope.Edit = function (personId) {

    //    window.location = "/Masters/RegistrationEntry?id=" + personId;
    //}
    $scope.Edit = function () {
        if ($scope.id > 0) {
            window.location = "/Masters/RegistrationEntry?id=" + $scope.id;
        }
        else {
            alert("Please Select The Person.")
        }
    }
    $scope.ActiveOrDeactiveList = function () {
        if (ListType == 1) {
            var ListId = 2;
            window.location = '/Masters/RegistrationList?Type=' + ListId;

        }
        else {
            var ListId = 1;
            window.location = '/Masters/RegistrationList?Type=' + ListId;

        }

    }
});
