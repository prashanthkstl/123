﻿
//-----------------------------------------------------------SELFCHITS ----------------------------------------------------------------//



var app = angular.module("ChitCareSelfChitsApp", []);
app.controller("ChitCareSelfChitsController", function ($scope,$http,$window) {

    $scope.btnSaveShow = false;
    var GrpId = 0;
    var PrId = 0;
    var chitNum = 0;
    var SPrId = 0;
    var ListNo = 0;
    var Enroll = 0;
    var IsDeleted = false;
    var SelfChitsListArray = {};
   
    $window.onload = function ()
    {
       
        $http.get('/Common/GetBranches').success(function (branchesResponse) {
            
            $scope.ddlBranch = {};
           
            $scope.ddlBranch.Brancesddl = branchesResponse;
            
            $scope.ddlBranch.BranchId = $scope.ddlBranch.Brancesddl[0].BranchId;

        })
        if (SelfPersonId > 0)
        {


        }
    }



     // Persons AutoList and Add Existed SelfChits to List//
    $scope.PersonAutoList = function (string, Type) {

        var output = [];
        var BranchId = $scope.ddlBranch.BranchId;

        var CompanyId = 0;
        var Id = 0;
        var GroupId = 0;
        var UserId = 0;
        if (Type == 'SubName') {
            $scope.hidethisP = false;
            $scope.hidethisP1 = true;
            var Mode = 1;
            var prefix = $scope.txtSubName;
        }
        if (Type == 'SubName1') {
            $scope.hidethisP1 = false;
            $scope.hidethisP = true;
            var Mode = 4;
            var prefix = $scope.txtSubName1;
        }
       



        $http.get('/AutoComplete/PersonAutoList?BranchId=' + BranchId + ' &CompanyId=' + CompanyId + ' &Id=' + Id + ' &Prefix=' + prefix + '&GroupId=' + GroupId + '&UserId=' + UserId + ' &Mode=' + Mode).then(function (personsResponse) {


            if (personsResponse.data.length > 0) {
                $scope.PersonList = personsResponse.data;

            }
            else {
                if (Type == 'SubName') {
                    $scope.hidethisP = true;

                }
                if (Type == 'SubName1') {

                    $scope.hidethisP1 = true;

                }
               

            }
           
        });
        angular.forEach($scope.PersonList, function (person) {

            if (person.Name.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                //output.push(country.preFix);
                output.push(person);
            }
        });
        $scope.filterPerson = output;
    }

    $scope.fillPersonTextbox = function (Name, id, Type,data) {

        if (Type == 'Subscriber') {
            $scope.txtSubName = Name;
            $scope.hidethisP = true;

        }
        if (Type == 'Subscriber1') {
            $scope.txtSubName1 = Name;
            $scope.hidethisP1 = true;

        }
      



        if (id > 0) {
            if (Type == 'Subscriber') {
                SPrId = id;
                $http.get('/Masters/GetSelfChitsPerson?PersonId=' + id).success(function (chitsResponse) {
                    
                    SelfChitsListArray = chitsResponse.selfChitList;
                   
                    if (SelfChitsListArray.length > 0)
                    {
                        $scope.btnSaveShow = true;
                        $scope.selfChits = SelfChitsListArray;
                    }
                    $scope.txtListNo = chitsResponse.selfChitAddress.ListNo;
                    $scope.txtName = chitsResponse.selfChitAddress.PersonName + " S/O " + chitsResponse.selfChitAddress.CoGuardianName;
                    $scope.txtAddress = chitsResponse.selfChitAddress.Address + " " + chitsResponse.selfChitAddress.Address1 + " " + chitsResponse.selfChitAddress.Address2 + " " + chitsResponse.selfChitAddress.CityName + " " + chitsResponse.selfChitAddress.PinCode;
                });

            }
            if (Type == 'Subscriber1') {
                $scope.selfCitGropup = data.GroupName;
                $scope.txtTktNo = data.ChitNo;
                GrpId = data.GroupId;
                PrId = data.PersonId;
                chitNum = data.ChitNo;
                Enroll = data.EnrollId;
               
            }
            

        }
        else {

        }
    }


     // Groups AutoList//
    $scope.completeGroup = function (string) {
        $scope.hidethisgoup = false;
        var output = [];
        $http.get('/Common/GroupsAutoList?prefix=' + $scope.selfCitGropup + '&BranchId=' + $scope.ddlBranch.BranchId + '').then(function (groupsResponse) {


            if (groupsResponse.data.length > 0) {
                $scope.groupList = groupsResponse.data;

            }
            else {

                $scope.hidethisgoup = true;
            }
            
        });
        angular.forEach($scope.groupList, function (group) {

            if (group.GroupName.toLowerCase().indexOf(string.toLowerCase()) >= 0) {

                output.push(group);
            }
        });
        $scope.filterGroup = output;
    }

    $scope.fillTextboxGroup = function (groupName, id, ChitNumber) {
        ChitNo = ChitNumber;
        $scope.selfCitGropup = groupName;
        $scope.hidethisgoup = true;
        GroupId = id;
        if (id > 0) {
            GrpId = id;
        }
        else {

        }
    }


     // Function for Get Person By ChitNo and GroupId//
    $scope.GetPerson = function (ChitNumber)
    {
        
        if ($scope.txtTktNo == "")
        {
            $scope.txtSubName1 = "";
        }
        $http.get('/Masters/GetPersonByGroupId?GroupId=' + GrpId + "&ChitNo=" + ChitNumber).success(function (personGrpResponse) {
           
            if (personGrpResponse.Name != null) {
                $scope.txtSubName1 = personGrpResponse.Name;
            }
            else
            {
                $scope.txtSubName1 = "";
            }
            if (personGrpResponse.Name != null)
                {
            GrpId = personGrpResponse.GroupId;
            PrId = personGrpResponse.PersonId;
            chitNum = personGrpResponse.ChitNo;
            }
        });
    }

    
    // Function for Add SelfChits to ListArray//
    $scope.AddSelfChit = function ()
    {
        var CheckExistinLocalArry = false;
        $http.get('/Masters/CheckExist?SPersonId=' + SPrId + '&PersonId=' + PrId+'&EnrollId='+Enroll).success(function (response) {
          
            if (response == 1 || IsDeleted == true)
            {
                $http.get('/Masters/GetNewSelfChitDetails?PersonId=' + PrId + '&GroupId=' + GrpId + "&ChitNo=" + chitNum).success(function (newSelfChitResponse) {

                    newSelfChitResponse[0].SPersonId = SPrId;

                    angular.forEach(SelfChitsListArray, function (ChitSelf) {
                       
                        if (ChitSelf.EnrollmentId == newSelfChitResponse[0].EnrollmentId)
                        {
                            CheckExistinLocalArry = true;
                        }
                    })
                    if (CheckExistinLocalArry == false)
                    {
                        SelfChitsListArray.push(newSelfChitResponse[0]);
                    }
                    else {
                        alert("This Person is already Exist")
                    }
                   
                    $scope.selfChits = SelfChitsListArray;
                   
                });
            }
            else
            {
                alert("This Person is already Exist")
            }
        });
        
    }


     // Function for Insert SelfChits into DB//
    $scope.SaveSelfChits = function () {
        
        var SelfChitLists = [];
        
        $http.get('/Masters/GetListNo?BranchId=' + $scope.ddlBranch.BranchId).success(function (ListNum) {

            ListNo = ListNum;

            angular.forEach(SelfChitsListArray, function (ChitSelf) {
                var SelfChList = {};
                SelfChList.SelfID = 0;
                SelfChList.ListNo = ListNo;
                SelfChList.SPersonId = SPrId;
                SelfChList.PersonId = ChitSelf.PersonId;
                SelfChList.EnrollId = ChitSelf.EnrollmentId;
                SelfChList.CreatedBy = 11;
                SelfChList.CreatedDate = new Date();
                SelfChList.ModifiedBy = 11;
                SelfChList.ModifiedDate = new Date();
                SelfChList.IsGroupWise = 1;
                SelfChitLists.push(SelfChList);
               
               
            })
            console.log(SelfChitLists)
            var post = $http({
                method: "POST",
                url: "/Masters/SelfChitsEntry",
                dataType: 'json',
                data: { selfChitsInsert: SelfChitLists },
                headers: { "Content-Type": "application/json" }
            });

            post.success(function () {


                alert("SelfChits Inserted Succefully ")


                //window.location = '/Masters/RegistrationEntry'
            });
        });
      
       

    }


     // Function for Delete SelfChits //
    $scope.DeleteSelfChits = function (Enrlid)
    {
        var SelfChitLists = [];
        alert(Enrlid)
        angular.forEach(SelfChitsListArray, function (ChitSelf) {
            var SelfChList = {};
            SelfChList = ChitSelf;
            if (ChitSelf.EnrollmentId != Enrlid)
            {
                SelfChitLists.push(SelfChList);
            }
            


        })
        SelfChitsListArray = SelfChitLists;
        $scope.selfChits = SelfChitsListArray;
        console.log(SelfChitsListArray)
        IsDeleted = true;
    }
});



 //-----------------------------------------------------------SELFCHITS LIST----------------------------------------------------------------//



var appSList = angular.module("SelfChitListApp", []);

appSList.controller("SelfChitListController", function ($scope, $http, $window) {
    $scope.DateRange = [{ value: 1, text: "All" }, { value: 2, text: "Today" }, { value: 3, text: "Current Week" }, { value: 4, text: "Current Month" }, { value: 5, text: "Current Quarter" }, { value: 6, text: "Current Year" }, { value: 7, text: "Date to Date" }, { value: 8, text: "Yester Day" }, { value: 9, text: "Previous Year" }, { value: 10, text: "Previous Month" }];
    $scope.ddlConRange = $scope.DateRange[0];
    $scope.txtFromDate = new Date();
    $scope.txtToDate = new Date();
    $window.onload = function () {
        alert("Working...")
        $http.get('/Common/GetBranches').then(function (branchesResponse) {

            $scope.ddlBranch = {};

            $scope.ddlBranch.Brancesddl = branchesResponse.data;

            $scope.ddlBranch.BranchId = $scope.ddlBranch.Brancesddl[0].BranchId;

        })
    }
    $scope.GetDateRange = function () {
        if ($scope.ddlConRange.value == 7) {
            $scope.showDatetoDate = true;
        } else {
            $scope.showDatetoDate = false;
        }
    }
    $scope.GetSelfChitList = function ()
    {
        $http.get('/Masters/SelfChitsEdit?BranchId=' + $scope.ddlBranch.BranchId + '&Prefix=' + $scope.txtPersonName+'&From')
    }
});
