﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChitCare.UI.ViewModels.Models;
using System.Threading.Tasks;
using ChitCare.UI.Web.Controllers;

namespace ChitCare.UI.Web.Areas.Masters.Controllers
{
    public class CommonController : ChitCareController
    {
        // GET: Masters/Common
        public PartialViewResult Menu()
        {
            
            List<MenuViewModel> menuList = CHITCAREHttpClient.GetRequestSync<List<MenuViewModel>>("Menu/MainMenu");
            return PartialView(menuList);
        }
        public async Task<JsonResult> GetBranches()
        {
            return Json(await CHITCAREHttpClient.GetRequest<List<BranchViewModel>>("Common/Branches", null), JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GroupsAutoList(string prefix, int BranchId)
        {

            return Json(await CHITCAREHttpClient.GetRequest<List<GroupsViewModel>>("Common/AutoListGroups", multiFormURIQueryString: "Prefix=" + prefix + "&Id=" + BranchId), JsonRequestBehavior.AllowGet);
        }
    }
}