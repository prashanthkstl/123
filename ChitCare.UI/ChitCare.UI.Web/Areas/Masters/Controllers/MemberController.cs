﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using ChitCare.UI.Web.Controllers;
using ChitCare.UI.ViewModels.Models;
namespace ChitCare.UI.Web.Areas.Masters.Controllers
{
    public class MemberController : ChitCareController
    {
        // GET: Masters/Member
        public ActionResult RegistrationEntry(int id = 0)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> MemberRegistrationEntry(PersonViewModel personViewModel)
        {


            await CHITCAREHttpClient.SendRequest<PersonViewModel, string>("Persons/Insert", personViewModel);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MemberRegistrationList(int Type = 0)
        {
            ViewBag.ListType = Type;
            return View();
        }


        public async Task<JsonResult> GetMemberRegistrationList(string BranchId, string Prefix, DateTime Fdate, DateTime ToDate, int ListType)

        {

            string fdate = Fdate.ToString();
            string todate = ToDate.ToString();
            List<PersonsDataViewModel> pDataList = await CHITCAREHttpClient.GetRequest<List<PersonsDataViewModel>>("Persons/GetPersons", multiFormURIQueryString: "BranchId=" + BranchId + "&Prefix=" + Prefix + "&Fdate=" + fdate + "&ToDate=" + todate + "&ListType=" + ListType);
            return Json(pDataList, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> MemberRegistrationEdit(int PersonId)
        {

            return Json(await CHITCAREHttpClient.GetRequest<PersonViewModel>("Persons/EditPerson", PersonId), JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ActivateOrDeactivatePerson(string Type, int PersonId)
        {
            await CHITCAREHttpClient.GetRequest("Masters/ActivateOrDeactivatePerson", multiFormURIQueryString: "Type=" + Convert.ToInt32(Type) + "&PersonId=" + PersonId);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}