﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChitCare.UI.Common.Cache;
using ChitCare.UI.Common.SimpleHttpClient;
using Common.IoC;
namespace ChitCare.UI.Web.Controllers
{
    public class ChitCareController : Controller
    {
        // GET: ChitCare
        [SimpleIoCPropertyInject]
        public ICHITCAREHttpClient CHITCAREHttpClient { get; set; }
       
    }
}