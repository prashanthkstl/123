﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using ChitCare.UI.ViewModels.Models;

namespace ChitCare.UI.Web.Controllers
{
    public class MastersController : ChitCareController
    {
        // GET: Masters

        #region Master


        #region MEMBER REGISTRATION 
        public ActionResult RegistrationEntry(int id = 0)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> RegistrationEntry(PersonViewModel personViewModel)
        {


            await CHITCAREHttpClient.SendRequest<PersonViewModel, string>("Persons/Insert", personViewModel);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RegistrationList()
        {
            return View();
        }

       
        public async Task<JsonResult> GetRegistrationList(string BranchId, string Prefix, DateTime Fdate, DateTime ToDate)

        {
            string fdate = Fdate.ToString();
            string todate = ToDate.ToString();
            List<PersonsDataViewModel> pDataList = await CHITCAREHttpClient.GetRequest<List<PersonsDataViewModel>>("Persons/GetPersons", multiFormURIQueryString: "BranchId=" + BranchId + "&Prefix=" + Prefix + "&Fdate=" + fdate + "&ToDate=" + todate);
            return Json(pDataList, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> RegistrationEdit(int PersonId)
        {
            
            return Json(await CHITCAREHttpClient.GetRequest<PersonViewModel>("Persons/EditPerson", PersonId), JsonRequestBehavior.AllowGet);
        }




        #endregion


        #region Chit Group
        public ActionResult ChitGroupEntry(int id = 0)
        {
            ViewBag.id = id;
            return View();
        }


        [HttpPost]
        public async Task<JsonResult> ChitGroupEntry(GroupsViewModel groupViewModel)
        {
            await CHITCAREHttpClient.SendRequest<GroupsViewModel, string>("Groups/GroupEntry", groupViewModel);

            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ChitGroupList() 
        {
        return View();
        }




        public async Task<JsonResult> GetChitGroupList(int Branch, string GroupName, string ChitSeries, DateTime FromDate, DateTime ToDate)
        {
            List<GroupsViewModel> GroupsList = await CHITCAREHttpClient.GetRequest<List<GroupsViewModel>>("Groups/GroupView", multiFormURIQueryString: "Branch=" + Branch + "&GroupName=" + GroupName + "&ChitSeries=" + ChitSeries + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "");
            return Json(GroupsList, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ChitGroupEdit(int GroupId)
        {
            GroupsViewModel GroupEditList = await CHITCAREHttpClient.GetRequest<GroupsViewModel>("Groups/GroupEdit", multiFormURIQueryString: "GroupId=" + GroupId + "");
            return Json(GroupEditList, JsonRequestBehavior.AllowGet);
        }

        #endregion



        #region ENROLLMENTS

        public ActionResult EnrollmentEntry()
        {
            return View();

        }
        public async Task<JsonResult> GetVacantChits(int GroupId)
        {
            return Json(await CHITCAREHttpClient.GetRequest<List<EnrollmentsViewModel>>("Masters/GetVacantChits", GroupId), JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Places
        public ActionResult CountryEntry(int? id=0)
        {
            return View();
        }
       

        public async Task<JsonResult> GetCountries()
        {

            List<CountriesViewModel> countries = await CHITCAREHttpClient.GetRequest<List<CountriesViewModel>>("Places/GetCountries");
            return Json(countries, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> CountryEdit(int CountryId)
        {
            CountriesViewModel CountryEditList = await CHITCAREHttpClient.GetRequest<CountriesViewModel>("Places/CountryEdit", multiFormURIQueryString: "CountryId=" + CountryId + "");
            return Json(CountryEditList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> CountryEntry(CountriesViewModel countriesViewModel)
        {
            await CHITCAREHttpClient.SendRequest<CountriesViewModel, string>("Places/CountryInsert", countriesViewModel);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CountryList()
        {
            return View();
        }
        public async Task<JsonResult> CountryUpdate(CountriesViewModel countriesViewModel)
        {

            await CHITCAREHttpClient.SendRequest<CountriesViewModel, string>("Places/UpdateCountry", countriesViewModel);


            return Json(countriesViewModel, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> CountryDestroy(CountriesViewModel countriesViewModel)
        {

            await CHITCAREHttpClient.SendRequest<CountriesViewModel, string>("Places/DeleteCountry", countriesViewModel);


            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #endregion





    }
}