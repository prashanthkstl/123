﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using ChitCare.UI.ViewModels.Models;
namespace ChitCare.UI.Web.Controllers
{
    public class CommonController : ChitCareController
    {
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> GetBranches()
        {
            return Json(await CHITCAREHttpClient.GetRequest<List<BranchViewModel>>("Common/Branches", null), JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GroupsAutoList(string prefix,int BranchId)
        {

            return Json(await CHITCAREHttpClient.GetRequest<List<GroupsViewModel>>("Common/AutoListGroups", multiFormURIQueryString: "Prefix=" + prefix+"&Id="+BranchId), JsonRequestBehavior.AllowGet);
        }
    }
}