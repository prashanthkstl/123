﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ChitCare.UI.ViewModels.Models;
namespace ChitCare.UI.Web.Controllers
{
    public class AutoCompleteController : ChitCareController
    {
        // GET: AutoComplete
        public ActionResult Index()
        {
            return View();
        }
        public async Task<JsonResult> CityAutoList(string prefix)
        {
            List<CityAListViewModel> Clist = await CHITCAREHttpClient.GetRequest<List<CityAListViewModel>>("Persons/CityAutoList", multiFormURIQueryString: "Prefix=" + prefix);
            return Json(Clist, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> GetPlacesByCityId(int CityId)
        {
            PlacesRequestViewModel Plist = await CHITCAREHttpClient.GetRequest<PlacesRequestViewModel>("Persons/GetPlacesByCityId", CityId);
            return Json(Plist, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> PersonAutoList(string BrnId, string Prefix, string CompId, string id, string GrpId, string Uid, string Mode)
        {

            List<PersonsAutoListViewModel> PersonsList = await CHITCAREHttpClient.GetRequest<List<PersonsAutoListViewModel>>("Persons/AutoList", multiFormURIQueryString: "BrnId=" + BrnId + "&CompId=" + CompId + "&id=" + id + "&Prefix=" + Prefix + "&GrpId=" + GrpId + "&Uid=" + Uid + "&Mode=" + Mode);


            return Json(PersonsList, JsonRequestBehavior.AllowGet);
        }
    }
}