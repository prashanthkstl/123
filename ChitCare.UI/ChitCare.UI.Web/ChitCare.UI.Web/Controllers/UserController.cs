﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ChitCare.UI.ViewModels.Models;
using Common;
using Dapper;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;
namespace ChitCare.UI.Web.Controllers
{
    public class UserController : ChitCareController
    {
        //GET: User
        private int m_UserId; string m_Uname; string m_Sal;
        
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Login(string userCode,string password)
        {

            int Result = 0;
            int PwdCheck = 0;
            try
            {
                LoginViewModel loginviewmodel = new LoginViewModel() { UserCode = userCode };
                loginviewmodel = await CHITCAREHttpClient.SendRequest<LoginViewModel, LoginViewModel>("User/UserValidate", loginviewmodel);
                m_Sal = loginviewmodel.Salutation.ToString();
                m_Uname = loginviewmodel.Uname.ToString();
                m_UserId = owutil.nvlint(loginviewmodel.UserId);
                Result = Int32.Parse(loginviewmodel.Result.ToString());
            }
            catch (Exception)
            {
                Response.Write("Invalid UserId");
                return View();
            }


            if (Result==1)
            {
                UserViewModel userViewModel = await CHITCAREHttpClient.GetRequest<UserViewModel>("User/GetUser", m_UserId);

                PwdCheck = CheckPassword(userViewModel.SaltValue,password,userViewModel.Password);
             
            }
            else
            {
                PwdCheck = 3;
            }

            if(PwdCheck==1)
            {
                UserViewModel userViewModel = await CHITCAREHttpClient.GetRequest<UserViewModel>("User/GetUser", m_UserId);
                short UserId = owutil.nvlshort(userViewModel.UserId);

                if(userViewModel.AccessType)
                {
                   
                   
                        IPCheckViewModel ipCheck = new IPCheckViewModel() { UserId = userViewModel.UserId.ToString(),IPAddress= Request.ServerVariables["REMOTE_ADDR"].ToString() };

                    ipCheck = await CHITCAREHttpClient.SendRequest<IPCheckViewModel, IPCheckViewModel>("User/IPAddressCheck", ipCheck);
                   if(!ipCheck.Allow)
                    {
                        //Label1.Text = "Unauthorised Accessing";
                        ////Label1.Text =Request.ServerVariables["REMOTE_ADDR"].ToString() + "  \n" + objloginfo.UserId.ToString() + " \n" ;
                        //Label1.Visible = true;
                        //return;
                    }


                }
                if(userViewModel.RoleId==4)
                    Session["Admin"] = "true";
                else
                    Session["Admin"] = "false";

                Session["UserId"] = userViewModel.UserId;
                Session["UserCode"] = userCode.Trim();
                Session["UserName"] = userViewModel.Salutation + "" + m_Uname;
                Session["Role"] = userViewModel.RoleId;
                Session["RoleName"] = userViewModel.RoleName;
                Session["FromLogin"] = "true";
                Session["Ipaddress"] = Request.ServerVariables["REMOTE_ADDR"].ToString();
                Session["AccessType"] = userViewModel.AccessType;


                return RedirectToAction("FinyearIndex", "Finyear");
            }
            if(PwdCheck==2)
            {
                Response.Write("Invalid Password");
                return View();
            }
            return View();
        }


        [NonAction]
        public int CheckPassword(byte[] SaltValue, string Password,byte[] dbPassword)
        {
            int result=0;
            Byte[] hashPwd = CreateSaltedPassword(SaltValue, Getbytes(Password));
            if (CompareByteArray(hashPwd, dbPassword) == true)
           
            {
                result= 1; //password are valid
            }
            else
            {
                result= 2;//Invalid Password
            }
            return result;
        }
        [NonAction]
        public byte[] Getbytes(string str)
        {
            //Function Converts String into Bytes
            UnicodeEncoding byteConverter = new UnicodeEncoding();
            return byteConverter.GetBytes(str);
        }
        [NonAction]
        public byte[] CreateSaltedPassword(byte[] saltValue, byte[] unsaltedPassword)
        {
            //			Add the salt to the hash.
            byte[] rawSalted = new byte[unsaltedPassword.Length + saltValue.Length];
           
            unsaltedPassword.CopyTo(rawSalted, 0);
            saltValue.CopyTo(rawSalted, unsaltedPassword.Length);
            //			Create the salted hash.         
            SHA1 sha1 = SHA1.Create();
            byte[] saltedPassword = sha1.ComputeHash(rawSalted);
            //			Add the salt value to the salted hash.
            byte[] dbPassword = new byte[saltedPassword.Length + saltValue.Length];
            saltedPassword.CopyTo(dbPassword, 0);
            saltValue.CopyTo(dbPassword, saltedPassword.Length);
           
            return dbPassword;
        }
        [NonAction]
        public Boolean CompareByteArray(byte[] array1, byte[] array2)
        //	To compare Both Passwords
        {
            
            if (array1.Length != array2.Length)
            {
                return false;
            }
            int i;
            for (i = 0; i < array1.Length - 1; i++)
            {
                if (array1[i] != array2[i])
                {
                    return false;
                }
            }
            return true;
            
        }


    }
}