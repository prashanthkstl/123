﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChitCare.UI.ViewModels.Models;
using System.Threading.Tasks;
namespace ChitCare.UI.Web.Controllers
{
    public class MenuController : ChitCareController
    {
        // GET: Menu
        public  ActionResult Index()
        {
            List<MenuViewModel> menuList =  CHITCAREHttpClient.GetRequestSync<List<MenuViewModel>>("Menu/MainMenu");
            
            return View(menuList);
        }

       public PartialViewResult GetMenu()

        {
            List<MenuViewModel> menuList = CHITCAREHttpClient.GetRequestSync<List<MenuViewModel>>("Menu/MainMenu");

            return PartialView("GetMenu", menuList);
        }

    }
}