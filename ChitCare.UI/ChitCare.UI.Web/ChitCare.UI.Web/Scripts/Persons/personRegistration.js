var app = angular.module("myChitCare", []);

app.directive("fileinput", [function () {
    return {
        scope: {
            fileinput: "=",
            filepreview: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.fileinput = changeEvent.target.files[0];
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.filepreview = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(scope.fileinput);
            });
        }
    }
}]);
app.directive("signinput", [function () {
    return {
        scope: {
            signinput: "=",
            signpreview: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.signinput = changeEvent.target.files[0];
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.signpreview = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(scope.signinput);
            });
        }
    }
}]);
app.controller("MyChitCareController", function ($scope, $http, $window) {
    //app.controller('MyController', function ($scope, $http, $window) {
    $scope.reqMobile = /^[0-9]{10}$/;
    $scope.Aadhar = /^[0-9]{12}$/;
    $scope.email = /^([\w-]+(?:\.[\w-]+)*)@@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    $scope.GSTNO = /^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[0-9]{1}Z[0-9]{1}?$/;
    $scope.PanValid = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
    var Member = 0;
    var Agent = 1;
    var Staff = 1;
    var Garuntor = 0;

    $scope.Empshow = true;
    $scope.txtPrDOB = new Date();

    $scope.txtBasicSal = 0.00;
    $scope.txtNetSal = 0;
    $scope.txtHRA = 0;
    $scope.txtOtherSal = 0;
    $scope.txtPDA = 0;
    $scope.txtDeductions = 0;
    $scope.txtInc = 0;
    $scope.txtCapital = 0;
    $scope.txtIncome = 0;

    $scope.AgentList = [{ value: 0, text: "Main" }, { value: 1, text: "Sub-Agent" }];
    $scope.SDOList = [{ value: "S/O", text: "S/O" }, { value: "D/O", text: "D/O" }, { value: "C/O", text: "C/O" }, { value: "M/S", text: "M/S" }, { value: "W/O", text: "W/O" }];
   
    $scope.ddlOccupation = [{ value: 0, text: "Employee" }, { value: 1, text: "Bussiness" }, { value: 2, text: "Others" }, { value: 3, text: "House Wife" }, { value: 4, text: "Rett. Off." }, { value: 5, text: "Farmer" }];
    $scope.ddlPrOcc = $scope.ddlOccupation[0];
   
    $scope.ddlSdo = $scope.SDOList[0];
    $scope.ddlAgntType = $scope.AgentList[0];
    $scope.HonorDdl = [{ value: 1, text: "Mr." }, { value: 2, text: "Mrs." }, { value: 3, text: "Miss." }, { value: 4, text: "Dr." }, { value: 5, text: "M/S." }, { value: 6, text: "Mnr." }];
    $scope.ddlPrHonor = $scope.HonorDdl[0];
   

    $scope.init = function () {

        $http.get('/Common/GetBranches').then(function (branchesResponse) {

           
            $scope.ddlBranch = {};
           
            $scope.ddlBranch.Brancesddl = branchesResponse.data;
            
            $scope.ddlBranch.BranchId = $scope.ddlBranch.Brancesddl[0].BranchId;

        })
        if (PersonId > 0)
        {
           
            $http.get('/Masters/RegistrationEdit?PersonId=' + PersonId).then(function (personResponse) {

                
               
         
                angular.forEach(personResponse.data.Address, function (value) {
                    
                   
                    if (value.AddressType == 1)
                    {


                        $scope.txtResAddress = value.Address;
                        $scope.txtResAddress1 = value.Address1;
                        $scope.txtResAddress2 = value.Address2;
                        $scope.txtRDistrict = value.DistrictId;
                        $scope.txtRCountry = value.CountryId;
                        $scope.City = value.CityId;
                        $scope.txtRPincode = value.PinCode;
                        $scope.txtRPhone = value.PhoneNo1;
                        $scope.txtRState = value.StateId;;

                    }
                    if (value.AddressType == 2) {


                        $scope.txtOffAddress = value.Address;
                        $scope.txtOffAddress1 = value.Address1;
                        $scope.txtOffAddress2 = value.Address2;
                        $scope.txtODistrict = value.DistrictId;
                        $scope.txtOCountry = value.CountryId;
                        $scope.OCity = value.CityId;
                        $scope.txtOPincode = value.PinCode;
                        $scope.txtOPhone1 = value.PhoneNo1;
                        $scope.txtOPhone2 = value.PhoneNo2;
                        $scope.txtOState = value.StateId;
                        $scope.txtOFax = value.Fax;


                    }
                    if (value.AddressType == 3) {


                        $scope.txtNatAddress = value.Address;
                        $scope.txtNatAddress1 = value.Address1;
                        $scope.txtNatAddress2 = value.Address2;
                        $scope.txtNDistrict = value.DistrictId;
                        $scope.txtNCountry = value.CountryId;
                        $scope.NCity = value.CityId;
                        $scope.txtNPincode = value.PinCode;
                        $scope.txtNPhone1 = value.PhoneNo1;
                        $scope.txtNState = value.StateId;;
                        $scope.txtNPhone2 = value.PhoneNo2;
                    }

                })


                $scope.txtCmpName = personResponse.data.personOccViewModel.CompanyName;
                $scope.txtDesg = personResponse.data.personOccViewModel.Designation;
                $scope.txtDept = personResponse.data.personOccViewModel.Department;
                $scope.txtPFNo = personResponse.data.personOccViewModel.EmployeeId;
                $scope.txtDOJ = new Date(personResponse.data.personOccViewModel.EmployeeJoinDate.match(/\d+/)[0] * 1);
                $scope.txtRetireDate = new Date(personResponse.data.personOccViewModel.RetireDate.match(/\d+/)[0] * 1);
                $scope.txtBasicSal = personResponse.data.personOccViewModel.Salary;
                $scope.txtHRA = personResponse.data.personOccViewModel.HRA;
                $scope.txtPDA = personResponse.data.personOccViewModel.DA;
                $scope.txtOtherSal = personResponse.data.personOccViewModel.OtherAllawances;
                $scope.txtDeductions = personResponse.data.personOccViewModel.Dedutions;
                $scope.txtNetSal = personResponse.data.personOccViewModel.NetSalary;
                $scope.txtTan = personResponse.data.personOccViewModel.TAN;
                $scope.txtFirmName = personResponse.data.personOccViewModel.FirmName;
                $scope.txtCapital = personResponse.data.personOccViewModel.Capital;
                $scope.txtIncome = personResponse.data.personOccViewModel.Income;
                $scope.txtNOB = personResponse.data.personOccViewModel.NOB; 


                $scope.ddlBranch.BranchId = personResponse.data.BranchId;
                $scope.txtPrDOB = new Date(personResponse.data.DOB.match(/\d+/)[0] * 1);
                $scope.txtUserCode = personResponse.data.PersonCode;
                $scope.txtPrEntryDate = new Date(personResponse.data.RegistrationDate.match(/\d+/)[0] * 1);
                $scope.txtGSTNO = personResponse.data.GSTNo;
                $scope.txtAdrCrdNo = personResponse.data.AdhaarCardNo;
                $scope.txtEmail = personResponse.data.Email;

                $scope.txtMobile = personResponse.data.CellNo;
                //$scope.ddlPrHonor = personResponse.data.Honor;
                $scope.txtPrFName = personResponse.data.PersonName;
                $scope.txtIntroLn = personResponse.data.SurName;
                $scope.txtPrSDo = personResponse.data.CofGaurdianName;
                $scope.rbGender = personResponse.data.Gender;
               
                $scope.txtITpan1 = personResponse.data.PanNo;
                $scope.txtPrOcc= personResponse.data.Occupation;
                $scope.filepreview = personResponse.data.Signature;
                $scope.signpreview = personResponse.data.PersonPhoto;
                $scope.txtGuardName = personResponse.data.GuardianName;
                $scope.txtGuarRelation = personResponse.data.GuardianRelation;
                $scope.txtIntroFn = personResponse.data.InroducerName;
                $scope.chkMember = personResponse.data.IsMember;
                $scope.chkAgent = personResponse.data.IsAgent;
                $scope.chkStaff = personResponse.data.IsStaff;
                $scope.chkGaruntor = personResponse.data.IsGaruntor;
                
                


            });
        }
    };

    $scope.PersonList = function ()
    {

        window.location = '/Masters/RegistrationList';
    }
    $scope.SubmitData = function () {
       
       
      
        var personOccVModel =
            {


                OccupationType: 1,
                CompanyName: $scope.txtCmpName,
                Designation: $scope.txtDesg,
                Department: $scope.txtDept,
                EmployeeId: $scope.txtPFNo,
                EmployeeJoinDate: new Date($scope.txtDOJ),
                RetireDate: new Date($scope.txtRetireDate),
                Salary: $scope.txtBasicSal,
                HRA: $scope.txtHRA,
                DA: $scope.txtPDA,
                OtherAllawances: $scope.txtOtherSal,
                Dedutions: $scope.txtDeductions,
                NetSalary: $scope.txtNetSal,
                TAN: $scope.txtTan,
                FirmName: $scope.txtFirmName,
                Capital: $scope.txtCapital,
                Income: $scope.txtIncome,
                NOB: ""
            };

        var Addres = [{

            AddressType: 1,
            Address: $scope.txtResAddress,
            Address1: $scope.txtResAddress1,
            Address2: $scope.txtResAddress2,
            CountryId: 1,
            StateId: 1,
            DistrictId: 1,
            CityId: 1,
            PinCode: "",
            PhoneNo1: $scope.txtRPhone,
            PhoneNo2: "",
            Fax: ""

        }, {
            AddressType: 2,
            Address: $scope.txtOffAddress,
            Address1: $scope.txtOffAddress1,
            Address2: $scope.txtOffAddress2,
            CountryId: 1,
            StateId: 1,
            DistrictId: 1,
            CityId: 1,
            PinCode: "",
            PhoneNo1: $scope.txtOPhone1,
            PhoneNo2: $scope.txtOPhone2,
            Fax: $scope.txtOFax

        }, {


            AddressType: 3,
            Address: $scope.txtNatAddress,
            Address1: $scope.txtNatAddress1,
            Address2: $scope.txtNatAddress2,
            CountryId: 1,
            StateId: 1,
            DistrictId: 1,
            CityId: 1,
            PinCode: "",
            PhoneNo1: $scope.txtNPhone1,
            PhoneNo2: $scope.txtNPhone2,
            Fax: ""

        }];

     
        var personViewModel =
            {

                PersonId:PersonId,
                BranchId: $scope.ddlBranch.BranchId,
                RegistrationDate: new Date($scope.txtPrEntryDate),
                PersonCode: $scope.txtUserCode,
                Password: $scope.txtPassword,
                SaltValue: $scope.txtPassword,
                Honor: $scope.ddlPrHonor,
                PersonName: $scope.txtPrFName,
                SurName: $scope.txtIntroLn,
                CofGaurdianName: $scope.txtPrSDo,
                Gender: $scope.rbGender,
                DOB: new Date($scope.txtPrDOB),
                PanNo: $scope.txtITpan1,
                Occupation: $scope.ddlPrOcc.value,
                Signature: $scope.filepreview,
                PersonPhoto: $scope.signpreview,
                GuardianName: $scope.txtGuardName,
                GuardianRelation: $scope.txtGuarRelation,
                InroducerName: $scope.txtIntroFn,
                IsMember: Member,
                IsAgent: Agent,
                IsStaff: Staff,
                IsGaruntor: Garuntor,
                GSTNo: $scope.txtGSTNO,
                AdhaarCardNo: $scope.txtAdrCrdNo,
                Email: $scope.txtEmail,
                PersonSatus: 0,
                MainAgentId: 1,
                GPS_Location: "",
                CollectionLimit: 0,
                LastLogin: new Date($scope.txtPrDOB),
                CreatedBy: 11,
                CreatedDate: new Date($scope.txtPrDOB),
                ModifiedBy: 11,
                ModifiedDate: new Date($scope.txtPrDOB),
                CellNo: $scope.txtMobile,
                Address: Addres,
                personOccViewModel: personOccVModel
            };
      

        var post = $http({
            method: "POST",
            url: "/Masters/RegistrationEntry",
            dataType: 'json',
            data: { personViewModel: personViewModel },
            headers: { "Content-Type": "application/json" }
        });

        post.success(function () {
            alert("Registration Succefully Done!!")
            window.location='/Masters/RegistrationEntry'
        });

        post.error(function (data, status) {
            $window.alert(data.Message);
        });
    }
    $scope.complete = function (string,Type) {

        if (Type == 'RCity')
        {
            $scope.hidethisO = false;
            $scope.hidethis = false;
            $scope.hidethisN = false;
            var Prefix = $scope.City;
        }
        if (Type == 'NCity') {
            $scope.hidethisO = false;
            $scope.hidethis = true;
            $scope.hidethisN = true;
            var Prefix = $scope.NCity;
        }
        if (Type == 'OffCity') {
            $scope.hidethisO = true;
            $scope.hidethis = true;
            $scope.hidethisN = false;
            var Prefix = $scope.OCity;
        }
        
     
        var output = [];



        $http({
            url: '/AutoComplete/CityAutoList',
            method: "POST",
            data: {
                
                prefix: Prefix
                
            },
            headers: { 'Content-Type': 'application/json; charset=utf-8' }
        })
            .then(function (response) {


                if (response.data.length>0) {
                    $scope.cityList = response.data;
                }
                else {
                    if (Type == 'RCity') {
                       
                        $scope.hidethis = true;
                        $scope.txtRCountry = "";
                        $scope.txtRDistrict = "";
                        $scope.txtRPincode = "";
                        $scope.txtRState = "";
                    }
                    if (Type == 'NCity') {
                        
                        $scope.hidethisN = false;
                        $scope.txtNCountry = "";
                        $scope.txtNDistrict = "";
                        $scope.txtNPincode = "";
                        $scope.txtNState = "";
                       
                    }
                    if (Type == 'OffCity') {
                        $scope.hidethisO = false;
                        $scope.txtOCountry = "";
                        $scope.txtODistrict = "";
                        $scope.txtOPincode = "";
                        $scope.txtOState = "";
                        
                    }
                  
                    
                   
                    
                }
               // $scope.cityList = response.data;
            },
            function (response) { // optional
                $scope.hidethis = true;
                $scope.autolistId = 0;
                console.log("Failed");
            });





        angular.forEach($scope.cityList, function (city) {

            if (city.CityName.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                //output.push(country.preFix);
                output.push(city);
            }
        });
        $scope.filterCity = output;


    }
  
    $scope.fillTextbox = function (string, id, AddrsType) {

        if (AddrsType == 'ResidenceAddr') {
            $scope.City = string;
            $scope.autolistId = id;
            $scope.hidethis = true;
        }
        if (AddrsType == 'CorrespAddr') {
            $scope.NCity = string;
            $scope.autolistId = id;
            $scope.hidethisN = false;
        }
        if (AddrsType == 'OffAddr') {
            $scope.OCity = string;
            $scope.autolistId = id;
            $scope.hidethisO = false;
        }
            
        
       
        if (id > 0)
        {
            GetPlaces(id, AddrsType);
        }
        else {
            if (AddrsType == 'ResidenceAddr')
            {
                $scope.txtRCountry = "";
                $scope.txtRDistrict = "";
                $scope.txtRPincode = "";
                $scope.txtRState = "";
            }
            if (AddrsType == 'CorrespAddr') {
                $scope.txtNCountry = "";
                $scope.txtNDistrict = "";
                $scope.txtNPincode = "";
                $scope.txtNState = "";
            }
            if (AddrsType == 'OffAddr') {
                $scope.txtOCountry = "";
                $scope.txtODistrict = "";
                $scope.txtOPincode = "";
                $scope.txtOState = "";
            }
        }
    }
    GetPlaces = function (id, AddrsType)
    {
        $http.get('/AutoComplete/GetPlacesByCityId?CityId=' + id).then(function (data) {

          
            if (AddrsType == 'ResidenceAddr') {
                $scope.txtRCountry = data.data.countryName;
                $scope.txtRDistrict = data.data.DistName;
                $scope.txtRPincode = data.data.PinCode;
                $scope.txtRState = data.data.StateName;
            }
            if (AddrsType == 'CorrespAddr') {
                $scope.txtNCountry = data.data.countryName;
                $scope.txtNDistrict = data.data.DistName;
                $scope.txtNPincode = data.data.PinCode;
                $scope.txtNState = data.data.StateName;
            }
            if (AddrsType == 'OffAddr') {
                $scope.txtOCountry = data.data.countryName;
                $scope.txtODistrict = data.data.DistName;
                $scope.txtOPincode = data.data.PinCode;
                $scope.txtOState = data.data.StateName;
            }
           
        });
    }
    $scope.GetDOB = function () {
        var date = new Date();
        var Rdate = new Date();
        if ($scope.txtPrAge == "") {
            $scope.txtPrDOB = date;
            $scope.txtRetireDate = '';
        }
        else {
            $scope.txtPrDOB = new Date(date.setYear(date.getFullYear() - parseInt($scope.txtPrAge)));

            $scope.txtRetireDate = new Date(Rdate.setYear(Rdate.getFullYear() - parseInt($scope.txtPrAge) + 58));
        }
    }
    $scope.UnCheckAll = function () {
        if ($scope.chkAgent) {
            Agent = 1;
            $scope.AgentType = true;
        } else {
            $scope.AgentType = false;
            $scope.MainAgentType = false;
            Agent = 1;
        }
        if ($scope.chkMember) {

            Member = 0;
        } else {
            Member = 0;
        }
        if ($scope.chkStaff) {
            Staff = 1;
           
        } else {
            Staff = 0;
        }
        if ($scope.chkGaruntor) {
            Garuntor = 1;
           
        } else {
            Garuntor = 0;
        }
    }
    $scope.GetAgent = function () {

        if ($scope.ddlAgntType.value == 1) {
            $scope.MainAgentType = true;
        }
        else
            $scope.MainAgentType = false;
    }

    $scope.GetOccId = function () {



        if ($scope.ddlPrOcc.value == 0) {
            $scope.Empshow = true;
            $scope.Bussshow = false;
        }
        else if ($scope.ddlPrOcc.value == 1) {
            $scope.Empshow = false;
            $scope.Bussshow = true;
        }
        else {
            $scope.Empshow = false;
            $scope.Bussshow = false;
        }

    }
    $scope.ddlHonor = function () {

        if ($scope.ddlPrHonor.value == 6) {
            $scope.txtGuard = true;
            $scope.txtRep = false;
        }
        else if ($scope.ddlPrHonor.value == 5) {
            $scope.txtGuard = false;
            $scope.txtRep = true;

        }
        else {
            $scope.txtGuard = false;
            $scope.txtRep = false;
        }
    }

    $scope.chkFillOAddr1 = function () {
        if ($scope.chkFillOAddr) {

            $scope.txtOffAddress = $scope.txtResAddress;
            $scope.txtOffAddress1 = $scope.txtResAddress1;
            $scope.txtOffAddress2 = $scope.txtResAddress2;
            $scope.OCity = $scope.City;
            $scope.txtODistrict = $scope.txtRDistrict;
            $scope.txtOState = $scope.txtRState;
            $scope.txtOCountry = $scope.txtRCountry;
            $scope.txtOPincode = $scope.txtRPincode;
            $scope.txtOPhone1 = $scope.txtRPhone;
            $scope.txtOPhone2 = "";
        }
        else {
            $scope.txtOffAddress = "";
            $scope.txtOffAddress1 = "";
            $scope.txtOffAddress2 = "";
            $scope.OCity = "";
            $scope.txtODistrict = "";
            $scope.txtOState = "";
            $scope.txtOCountry = "";
            $scope.txtOPincode = "";
            $scope.txtOPhone1 = "";
            $scope.txtOPhone2 = "";
        }


    }

    $scope.changeGroupA = function () {
        if ($scope.vm.chkFillCAddr) {

            $scope.txtNatAddress = $scope.txtResAddress;
            $scope.txtNatAddress1 = $scope.txtResAddress1;
            $scope.txtNatAddress2 = $scope.txtResAddress2;
            $scope.NCity = $scope.City;
            $scope.txtNDistrict = $scope.txtRDistrict;
            $scope.txtNState = $scope.txtRState;
            $scope.txtNCountry = $scope.txtRCountry;
            $scope.txtNPincode = $scope.txtRPincode;
            $scope.txtNPhone1 = $scope.txtRPhone;
            $scope.txtNPhone2 = "";
        }
        else if ($scope.vm.chkFillCAddrfrmoffaddr) {
            $scope.txtNatAddress = $scope.txtOffAddress;
            $scope.txtNatAddress1 = $scope.txtOffAddress1;
            $scope.txtNatAddress2 = $scope.txtOffAddress2;
            $scope.NCity = $scope.OCity;
            $scope.txtNDistrict = $scope.txtODistrict;
            $scope.txtNState = $scope.txtOState;
            $scope.txtNCountry = $scope.txtOCountry;
            $scope.txtNPincode = $scope.txtOPincode;
            $scope.txtNPhone1 = $scope.txtOPhone1;
            $scope.txtNPhone2 = $scope.txtOPhone2;
        }
        else {

            $scope.txtNatAddress = "";
            $scope.txtNatAddress1 = "";
            $scope.txtNatAddress2 = "";
            $scope.NCity = "";
            $scope.txtNDistrict = "";
            $scope.txtNState = "";
            $scope.txtNCountry = "";
            $scope.txtNPincode = "";
            $scope.txtNPhone1 = "";
            $scope.txtNPhone2 = "";

        }

    }
    $scope.GetDate = function () {
        var today = new Date();
        var datedob = new Date($scope.txtPrDOB);



        var date1 = Math.abs(today.getTime() - datedob.getTime());
        var Age = Math.ceil((date1 / (1000 * 3600 * 24)) / 365) - 1;
        $scope.txtPrAge = Age;
        var aa = datedob.getFullYear() + 58;
        var bb = new Date(datedob.setYear(datedob.getFullYear() + 58));
        $scope.txtRetireDate = bb;

    }
    $scope.GetNetSal = function () {
        $scope.txtNetSal = parseInt($scope.txtBasicSal) + parseInt($scope.txtHRA) + parseInt($scope.txtOtherSal) + parseInt($scope.txtPDA) - parseInt($scope.txtDeductions);
    }
    $scope.filterValue = function ($event) {
        if (isNaN(String.fromCharCode($event.keyCode))) {
            $event.preventDefault();
        }
    };
});
app.directive('onlyAlphabets', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^A-Za-z]+/g, '');
        
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

app.directive('alphaNumeric', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                var transformedInput = text.replace(/[^0-9a-z]+/g, '');
              
                if (transformedInput !== text) {
                    ngModelCtrl.$setViewValue(transformedInput);
                    ngModelCtrl.$render();
                }
                return transformedInput;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.filter('jsonDate', ['$filter', function ($filter) {
    return function (input, format) {
        return (input) ? $filter('date')(parseInt(input.substr(6)), format) : '';

    };
}]);