﻿var app = angular.module("PersonListApp", []);

app.controller("MyChitCarePersonList", function ($scope, $http, $window) {
    $scope.showListByPerson = true;
   
    $scope.DateRange = [{ value: 1, text: "All" }, { value: 2, text: "Today" }, { value: 3, text: "Current Week" }, { value: 4, text: "Current Month" }, { value: 5, text: "Current Quarter" }, { value: 6, text: "Current Year" }, { value: 7, text: "Date to Date" }, { value: 8, text: "Yester Day" }, { value: 9, text: "Previous Year" }, { value: 10, text: "Previous Month" }];
    $scope.Typesddl = [{ value: 1, text: "Person wise" }, { value: 2, text: "Group wise" }];
    $scope.Brancesddl = [{ value: 1, text: "MADDUR" }, { value: 2, text: "nizamabad" }];
    $scope.ddlBranch = $scope.Brancesddl[0];
    $scope.ddlSearchType = $scope.Typesddl[0];
    $scope.ddlConRange = $scope.DateRange[0];
   
    $scope.SearchType = function ()
    {
      
        if ($scope.ddlSearchType.value === 1)
        {
            $scope.showListByPerson = true;
            $scope.showListByGroup = false;
        } else 
        {
            $scope.showListByPerson = false;
            $scope.showListByGroup = true;
        }
    }
    $scope.GetDateRange = function ()
    {
        if ($scope.ddlConRange.value == 7)
        {
            $scope.showDatetoDate = true;
        } else {
            $scope.showDatetoDate = false;
        }
    }

    $scope.GePersons = function ()
    {
       

    }
});