﻿var app = angular.module('myEnrollMent', []);

app.controller('enrollmentController', function ($scope, $window, $http, $filter) {
    $scope.ddlPayMode = {};
    $scope.ddlICardType = {};
    $scope.ddlIntType = {};
    $scope.ddlIntType.IntemationTypes = [{ text: "Collection Agent", value: 1 }, { text: "Bussiness Agent", value: 2 }, { text: "Self Chits(GroupList)", value: 3 }];
    $scope.ddlICardType.IntemationCardTypes = [{ text: "Post", value: 1 }, { text: "Hand", value: 2 }, { text: "Courior", value: 3 }, { text: "EMail", value: 4 }, { text: "NO", value: 5 }];
    $scope.ddlICardType.value = $scope.ddlICardType.IntemationCardTypes[0].value;
    $scope.ddlPayMode.PayModes = [{ text: "Monthly", value: 1 }, { text: "Weekly", value: 2 }, { text: "Daily", value: 3 }];
    $scope.ddlPayMode.value = $scope.ddlPayMode.PayModes[0].value;
    $scope.ddlIntType.value = $scope.ddlIntType.IntemationTypes[0].value;
   
    $window.onload = function ()
    {

        

        $http.get('/Common/GetBranches').success(function (branchesResponse) {
            $scope.enrlBranch = {};
            $scope.ddlBranch = {};
            $scope.enrlBranch.Branches = branchesResponse;
            $scope.ddlBranch.Branches = branchesResponse;
            $scope.enrlBranch.BranchId = $scope.enrlBranch.Branches[0].BranchId;
            $scope.ddlBranch.BranchId = $scope.ddlBranch.Branches[0].BranchId;
            
        })
    }

    $scope.GetId = function () {

        alert($scope.enrlBranch.BranchId);
    }
    $scope.complete = function (string)
    {
        $scope.hidethis = false;
        var output = [];
        $http.get('/Common/GroupsAutoList?prefix=' + $scope.enrlGropup + '&BranchId=' + $scope.ddlBranch.BranchId + '').then(function (groupsResponse) {

           
            if (groupsResponse.data.length > 0) {
                $scope.groupList = groupsResponse.data;
                
            }
            else {
               
                $scope.hidethis = true;
            }
            console.log(groupsResponse.data);
        });
        angular.forEach($scope.groupList, function (group) {

            if (group.GroupName.toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                //output.push(country.preFix);
                output.push(group);
            }
        });
        $scope.filterGroup = output;
    }

    $scope.fillTextbox = function (groupName, id) {

        $scope.enrlGropup = groupName;
        $scope.hidethis = true;

        if (id > 0) {
            $http.get('/Masters/GetVacantChits?GroupId=' + id).then(function (response) {

                $scope.ddlEnlTkt = {};
                $scope.ddlEnlTkt.Tickets = response.data;
                $scope.ddlEnlTkt.ChitNo = $scope.ddlEnlTkt.Tickets[0].ChitNo;
            });
        }
        else {

        }
    }
});