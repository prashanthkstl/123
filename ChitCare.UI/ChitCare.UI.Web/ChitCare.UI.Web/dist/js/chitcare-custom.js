
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
		//var viewportHeight = $(window).height()-51;
		//$(".sidebar").css("min-height", viewportHeight);
		//$(".sidebar").css("max-height", viewportHeight);
		//$("#page-wrapper").css("height", viewportHeight);
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
			$(".sidebar").css("min-height", "0px");
			$("#page-wrapper").css("margin-left","0px");
			$("ul.nav-third-level").addClass("mobile-3rd-level");
			$("ul.nav-third-level").removeClass("nav-third-level");
        } else {
            $('div.navbar-collapse').removeClass('collapse');
			$("#page-wrapper").css("margin-left","250px");
			$("ul.mobile-3rd-level").addClass("nav-third-level");
			$("ul.mobile-3rd-level").removeClass("mobile-3rd-level");
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");

        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});

/*=== Toggled Menu ===*/

$("#sidenavToggler").click(function() {
	$("#side-menu").toggleClass("sidenav-toggled");
	$(".menu-item > span").toggle();
	
	if ($("#side-menu").hasClass("sidenav-toggled")){
		$(".sidebar").css("width","70px");
		$(".navbar-brand").css("width","0px");
		$("#page-wrapper").css("margin-left","70px");
	}
	else {
		$(".sidebar").css("width","250px");
		$("#page-wrapper").css("margin-left","250px");
		$(".navbar-brand").css("width","165px");
	}
	
	if ($("ul.nav-second-level").hasClass("in")){
		$("ul.nav-second-level").removeClass("in");
	}
	$("#side-menu > li.active").removeClass("active");
	$("#sidenavToggler").show();
});

/*=== Toggled Menu End ===*/


/*=== Matching nav-third-level Position ===*/
function mposition() {
	var e = $("#side-menu > li.active").position();
	return e;
}


$("#side-menu > li").click(function() {
	var m = mposition();
	var b = m.top + 51;
	if ($("#side-menu").hasClass("sidenav-toggled")){
		$(".nav-second-level").css("top",b);
	}
});

$(".nav-second-level > li").click(function() {
	
	$(this).toggleClass("nav-third-open");
	var p = $(this).position();
	var m = mposition();
	var a = m.top + p.top + 51;
	if ($(".nav-second-level > li").hasClass("nav-third-open")){
		$(".nav-third-open > .nav-third-level").css("top",a);
	}
	
});
/*=== Matching nav-third-level Position End ===*/

$("#page-wrapper, .navbar-static-top").click(function() {
	if($("ul.nav-third-level").hasClass("in")){
		$("ul.nav-third-level").removeClass("in");
	}
});

$("a.menu-item").click(function() {
	if($("ul.nav-third-level").hasClass("in")){
		$("ul.nav-third-level").removeClass("in");
	}
});

/* === Mobile Nav bg ==*/
$(".navbar-toggle").click(function() {
	$(".mobile-overlay").toggle();
});

/* === Theme Colors ==*/
$(".theme > a").click(function(){
	$(".color-options").slideToggle();
});

$(".color-btn").click(function() {
	$('body').removeClass();
	var colorId = this.id;
	
	if (colorId == 'red'){
		$('body').addClass("theme-red");
	}
	else if (colorId == 'blue'){
		$('body').addClass("theme-blue");
	}
	else if (colorId == 'orange'){
		$('body').addClass("theme-orange");
	}
	else if (colorId == 'green'){	
		$('body').addClass("theme-green");
	}
	else if (colorId == 'seegreen'){	
		$('body').addClass("theme-seegreen");
	} 
	
});

/* === Menu Color == */
$(".menu-color-chg").click(function(){
	var a = $('input[name=sidemenu]:checked').val();
	if(a == "sidemenu-dark") {
		$(".navbar").addClass("dark-sidemenu");
	}
	else {
		$(".navbar").removeClass("dark-sidemenu");
	}
});

$(".topmenu-color-chg").click(function(){
	var a = $('input[name=topmenu]:checked').val();
	if(a == "topmenu-dark") {
		$(".navbar").addClass("dark-topmenu");
	}
	else {
		$(".navbar").removeClass("dark-topmenu");
	}
});


/*===== Matching the height of the divs in same row ====*/
	
$(document).ready(function () {
      var parentHeight = $('.equals .equal-height').parent().height();
      $('.equals .equal-height').each( function () {
        $(this).css('height', parentHeight+'px');
      });
});

$(document).ready(function () {
      var parentHeight1 = $('.equals1 .equal-height1').parent().height();
      $('.equals1 .equal-height1').each( function () {
        $(this).css('height', parentHeight1+'px');
      });
});

$(document).ready(function () {
      var parentHeight2 = $('.equals2 .equal-height2').parent().height();
      $('.equals2 .equal-height2').each( function () {
        $(this).css('height', parentHeight2+'px');
      });
});

/*===== Tooltip ====*/

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

