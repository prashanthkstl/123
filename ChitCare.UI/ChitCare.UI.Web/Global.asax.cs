﻿using System.Web.Mvc;
using System.Web.Routing;
using ChitCare.UI.Common.Cache;
using ChitCare.UI.Common.SimpleHttpClient;
using Common.IoC;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using SimpleInjector.Integration.Web.Mvc;
using System.Web.Optimization;
using ChitCare.UI.Web.App_Start;
namespace ChitCare.UI.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RegisterSimpleIoC();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void RegisterSimpleIoC()
        {
            Container container = new Container();
            container.Options.AutowirePropertiesWithAttribute<SimpleIoCPropertyInjectAttribute>();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            // This is an extension method from the integration package.
            container.RegisterMvcControllers();

            #region Http client
            container.RegisterSingleton(typeof(ICHITCAREHttpClient), typeof(CHITCAREHttpClient));
            container.RegisterSingleton(typeof(IAspNetCacheManager), typeof(AspNetCacheManager));
            #endregion

            //Simple IOC web mvc dependency resolver.
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }
}
