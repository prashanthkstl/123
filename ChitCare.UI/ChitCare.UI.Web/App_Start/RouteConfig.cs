﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ChitCare.UI.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Masters", action = "RegistrationEntry", id = UrlParameter.Optional }
            //);
            routes.RouteExistingFiles = true;
            routes.MapRoute(
           name: "Default",
           url: "{area}/{controller}/{action}/{id}",
           defaults: new { area = "Masters", controller = "Member", action = "MemberRegistrationEntry", id = UrlParameter.Optional },
           namespaces: new[] { "ChitCare.UI.Web.Areas.Masters" }
       ).DataTokens.Add("area", "Masters");
        }
    }
}
